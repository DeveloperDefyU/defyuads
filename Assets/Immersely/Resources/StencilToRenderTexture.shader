Shader "Defyu/StencilToRT" {
    Properties {
		_StencilMask("Stencil Mask", Int) = 0		
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "RenderType"="Opaque"
            "CanUseSpriteAtlas"="True"
        }
        LOD 100

		

        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            ZTest Always
            ZWrite Off
            
            Stencil 
            {			
                Ref[_StencilMask]
                Comp Equal
            }
			
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
     
            #include "UnityCG.cginc"
           
            #pragma target 3.0

			int _StencilMask;
			
            struct VertexInput 
            {
                float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
            };

            struct VertexOutput 
            {
                float4 pos : SV_POSITION;				
				float2 uv : TEXCOORD0;				
            };

            VertexOutput vert (VertexInput v) 
            {
                VertexOutput o = (VertexOutput)0;

				#if UNITY_UV_STARTS_AT_TOP
				float scale = -1.0;
				#else
				float scale = 1.0;
				#endif

				o.pos = v.vertex * float4(2, 2, 0, 0) + float4(0, 0, 0, 1);
				o.pos.y *=scale;
				o.uv = v.texcoord;
				
                return o;
            }
            float4 frag(VertexOutput i) : COLOR 
            {				
				return _StencilMask/(float)255;             
            }
            ENDCG
        }
    }
}
