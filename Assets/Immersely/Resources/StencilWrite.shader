﻿Shader "Defyu/StencilWrite"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_StencilMask("Stencil Mask", Int) = 0		
	}
	
	SubShader
	{
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		LOD 100
		Blend SrcAlpha OneMinusSrcAlpha
		Pass
		{

			Stencil 
			{
				Ref[_StencilMask]			
				Comp Always
				Pass Replace
			}

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			
			#include "UnityCG.cginc"
			sampler2D _MainTex;
			int _WriteAll;
			float4 _MainTex_ST;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;				
				float4 vertex : SV_POSITION;
				float4 scrPos : TEXCOORD1;				
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);				
				return o;
			}			

			void frag (v2f i) 
			{			
				// sample the texture
				fixed4 col = _WriteAll==1 ? 1 : tex2D(_MainTex, i.uv);
				clip(col.a - 0.1f);				
			}
			ENDCG
		}
		
	}
}
