﻿using System.Collections.Generic;

[System.Serializable]
public class RequestBase
{
    public RequestBase()
    {
        ApplicationKey = string.Empty;
        ApplicationVersion = string.Empty;
        DeviceId = string.Empty;
        SessionKey = string.Empty;
        Platform = string.Empty;
        Language = string.Empty;
        Country = string.Empty;
        SessionTime = 0;
        SessionOrder = 0;

    }

    public string ApplicationKey;
    public string ApplicationVersion;
    public string DeviceId;
    public string Platform;
    public string Language;
    public string Country;
    public string SessionKey;
    public long SessionTime;
    public long SessionOrder;
}

[System.Serializable]
public class AssetRequest : RequestBase
{
    public AssetRequest()
    {
        PlacementKey = string.Empty;
    }

    public string PlacementKey;
}

[System.Serializable]
public class NotifyRequest : RequestBase
{
    public NotifyRequest()
    {
        PlacementKey = string.Empty;
        Tag = string.Empty;
        ScreenUsagePercentByTime = 0.0f;
    }

    public string PlacementKey;
    public float ScreenUsagePercentByTime;
    public string Tag;
}

[System.Serializable]
public enum WebApiResponseState
{
    Success = 0,
    Error = 1,
}

[System.Serializable]
public class BaseResponse
{
    public WebApiResponseState State;
}

[System.Serializable]
public enum ContentTypes
{
    Text,
    Texture,
    Numeric,
    Empty,
}

[System.Serializable]
public class AssetAnswer : BaseResponse
{
    public string Id;
    public string Url;
    public string Name;
    public string Description;
    public ContentTypes Type;
    public float Numeric;
    public string PlainText;
   // public string Hired;
}

[System.Serializable]
public class AllPlacementsAnswer : BaseResponse
{
    public AllPlacementsAnswer()
    {
        AssetAnswers = new List<AssetAnswer>();
    }
    public List<AssetAnswer> AssetAnswers;
}

[System.Serializable]
public class SessionEventResponse : BaseResponse
{
    public string SessionKey;
}