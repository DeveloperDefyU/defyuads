﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
public static class ClientData
{
    static ClientData()
    {
        appVersion = string.Empty;
        deviceId = string.Empty;
        applicationKey = string.Empty;
        sessionKey = string.Empty;
        platform = RuntimePlatform.OSXEditor;
        language = SystemLanguage.Unknown;
        country = string.Empty;
    }

    public static void Initialize()
    {
        appVersion = Application.version;
        deviceId = SystemInfo.deviceUniqueIdentifier;
        platform = Application.platform;
        language = Application.systemLanguage;
        country = "unknow";
    }

    public static RuntimePlatform platform { get; set; }
    public static string applicationKey { get; set; }
    public static string sessionKey { get; set; }
    public static string deviceId { get; private set; }
    public static string appVersion { get; private set; }
    public static SystemLanguage language { get; private set; }
    public static string country { get; private set; }

    public static void MergeWithRequestBase(RequestBase requestBase)
    {
        requestBase.ApplicationKey = applicationKey;
        requestBase.SessionKey = sessionKey;
        requestBase.DeviceId = deviceId;
        requestBase.ApplicationVersion = appVersion;
        requestBase.Platform = platform.ToString();
        requestBase.Language = language.ToString();
        requestBase.Country = country;
    }
}