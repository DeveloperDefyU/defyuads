﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Immersely
{
    [Serializable]
    public class AssetObjectContent : ScriptableObject
    {

        #region fields

        public DownloadStatus DownloadStatus;
        public Resolution CurrentResolution = Resolution.SQ;
        // private string _text;
        // private Sprite _sprite;
        [SerializeField]
        private Texture2D _texture;
        // private float _numeric;
        [SerializeField]
        private ContentTypes _type;
        [SerializeField]
        private string _id;
        [SerializeField]
        private string _url;
        [SerializeField]
        private string _name;
        [SerializeField]
        private string _description;

        public bool Contracted;

        #endregion

        #region public properties
        public ContentTypes ContentType { get { return _type; } }

        public Texture2D Texture
        {
            get
            {
                if (_type != ContentTypes.Texture)
                {
                    Debug.LogError("Asked for sprite but type is: " + _type.ToString());
                    return null;
                }

                return _texture;
            }

            set
            {
                _texture = value;
            }
        }
        /*
        public string Text { get
            {
                if (_type != ContentTypes.Text)
                {
                    Debug.LogError("Asked for sprite but type is: " + _type.ToString());
                    return null;
                }

                return _text;
            } }
        */
        public bool NeedsDownload
        {
            get
            {
                switch (ContentType)
                {

                    case ContentTypes.Texture:
                        return true;

                    default:
                        return false;
                }
            }
        }

        public string Path { get { return _url; } }

        public string Key { get { return _id; } }

        public string Name { get { return _name; } }

        public string Description { get { return _description; } }

        public bool IsEmpty { get { return _type == ContentTypes.Empty; } }

        public bool HasMaterial { get { return _type == ContentTypes.Texture; } }

        public bool HasKey { get { return Key.Length > 0; } }


        #endregion

        #region constructor

        public void SetAssetObjectContent(AssetAnswer answer)
        {
            //SetValues(answer.Id, answer.Type, answer.Content);

            _type = answer.Type;
            _id = answer.Id;

            _name = answer.Name;
            _description = answer.Description;

            //Hack while type not coming from server
            if (_id == "6f18197c7d424ccfab31acb511c4fbef")
                _type = ContentTypes.Empty;

            switch (_type)
            {
                case ContentTypes.Texture:
                    SetForDownloadTexture(answer.Url);
                    break;
            }

        }

        #endregion

        #region download

        private void SetForDownloadTexture(string url)
        {
            _url = url;
        }

        internal void DownloadFinished(Texture2D texture2D)
        {
            _texture = texture2D;
            DownloadStatus = DownloadStatus.Done;
            MaterialManager.AddContent(this);
        }

        public void DownLoadFailed()
        {
            DownloadStatus = DownloadStatus.Failed;
        }

        internal void SetWaiting()
        {
            DownloadStatus = DownloadStatus.Waiting;
        }

        internal void SetDownloadInProgress()
        {
            DownloadStatus = DownloadStatus.Processing;
        }

        #endregion
    }

    #region enums

    public enum DownloadStatus
    {
        None,
        Waiting,
        Processing,
        Done,
        Failed,
    }

    public enum Resolution
    {
        SQ,
        L_4_3,
        L_2_1,
        L_16_9,

    }

    #endregion

}