﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif


[ExecuteInEditMode]
public class WebApiClient : MonoBehaviour
{
    //[System.NonSerialized]
    //public string applicationKey = string.Empty;
   // public string domain = @"http://localhost:54849";
    public string domain = @"http://lasse.defyu.com";
    public string baseUrl { get { return string.Format(@"{0}/api", domain); } }

    private static WebApiClient _instance;
    private static WebApiClient instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<WebApiClient>();

                if (_instance == null)
                {
                    GameObject go = new GameObject();
                    go.name = "WebApiClient";
                    _instance = go.AddComponent<WebApiClient>();
                }
            }

            return _instance;
        }
    }

    public delegate void ClientResponseCallback<T>(bool success, T entity);
    delegate void ServerResponseCallback<T>(bool success, string json, ClientResponseCallback<T> clientCallback);
    public delegate void ClientResponse<T>(bool success, T entity);
    delegate void ResponseCallback(bool success, string json);

    private int RequestCount { get; set; }

#if OFUSCATE
#else
    const string COMMAND_SESSION_START = "sessionstart";
    const string COMMAND_SESSION_END = "sessionend";
    const string COMMAND_ASSET_OBJECTS = "assetobjects";
    const string COMMAND_ASSET_OBJECT = "assetobject";
    const string COMMAND_SCREEN_USAGE = "screenusage";
#endif

    public static void Initialize()
    {

        instance._Initialize();
    }

    private void _Initialize()
    {
       // DontDestroyOnLoad(this);
        ClientData.Initialize();
        RequestCount = 0;

#if UNITY_EDITOR
        EditorApplication.update += Update;
#endif
    }




    void Update()
    {
        TriggerEnqueuedActions();
    }

    public static void Request_SessionStart(ClientResponseCallback<SessionEventResponse> callback)
    {
        string url = string.Format(@"{0}/{1}", instance.baseUrl, COMMAND_SESSION_START);
        var request = new RequestBase();
        ClientData.sessionKey = string.Empty;
        instance.Request(url, instance.SessionStartServerCallback, callback, request, RequestMethod.POST);
    }

    public static void Request_SessionEnd()
    {
        string url = string.Format(@"{0}/{1}", instance.baseUrl, COMMAND_SESSION_END);
        var request = new RequestBase();
        instance.Request(url, request, RequestMethod.POST);
    }

    public static void Request_AllPlacements(ClientResponseCallback<AllPlacementsAnswer> callback)
    {
        string url = string.Format(@"{0}/{1}", instance.baseUrl, COMMAND_ASSET_OBJECTS);
        var request = new RequestBase();
        instance.Request(url, instance.AllPlacementsServerCallback, callback, request, RequestMethod.POST);
    }

    public static void Request_Asset(string placementKey, ClientResponseCallback<AssetAnswer> callback)
    {
        string url = string.Format("{0}/{1}", instance.baseUrl, COMMAND_ASSET_OBJECT);
        var request = new AssetRequest();
        request.PlacementKey = placementKey;
        instance.Request(url, instance.AssetServerCallback, callback, request, RequestMethod.POST);
    }

    public static void Request_ScreenUsagePercentByTime(string placementKey, float screenUsagePercentByTime, string tag = "")
    {
        string url = string.Format("{0}/{1}", instance.baseUrl, COMMAND_SCREEN_USAGE);
        var request = new NotifyRequest();
        request.PlacementKey = placementKey;
        request.ScreenUsagePercentByTime = screenUsagePercentByTime;
        instance.Request(url, request, RequestMethod.POST);
    }


    const string COMMAND_EDITOR_ASSET_OBJECTS = "editor/assetobjects";

    public static void Request_AllPlacementsForEditor(ClientResponseCallback<AllPlacementsAnswer> callback)
    {
        string url = string.Format(@"{0}/{1}/{2}", instance.baseUrl, COMMAND_EDITOR_ASSET_OBJECTS, ClientData.applicationKey);
        instance.Request(url, instance.AllPlacementsServerCallback, callback, null, RequestMethod.GET);
    }


    void AllPlacementsServerCallback(bool success, string json, ClientResponseCallback<AllPlacementsAnswer> clientCallback)
    {
        if (clientCallback != null)
        {
            var entity = JsonUtility.FromJson<AllPlacementsAnswer>(json);
            clientCallback(success, entity);
        }
    }

    void AssetServerCallback(bool success, string json, ClientResponseCallback<AssetAnswer> clientCallback)
    {
        if (clientCallback != null)
        {
            var entity = JsonUtility.FromJson<AssetAnswer>(json);
            clientCallback(success, entity);
        }
    }

    void SessionStartServerCallback(bool success, string json, ClientResponseCallback<SessionEventResponse> clientCallback)
    {
        var entity = JsonUtility.FromJson<SessionEventResponse>(json);
        ClientData.sessionKey = entity.SessionKey;
        clientCallback(success, entity);
    }

    #region Thread

    delegate void QueueAction();
    static Queue<QueueAction> queue = new Queue<QueueAction>();

    enum RequestMethod
    {
        GET,
        POST
    }

    void TriggerEnqueuedActions()
    {
        if (queue.Count > 0)
        {
            while (queue.Count > 0)
            {
                QueueAction action = queue.Dequeue();
                action();
            }
        }
    }

    void Request(string url, RequestBase request, RequestMethod? method = null)
    {
        if (!readyToRequest)
        {
            Debug.LogErrorFormat("Set Application Key!!");
            return;
        }

        SetRequestBase(request);

        Thread t;
        t = new Thread(() => ThreadedRequest(url, request, method));
        t.Start();

#if UNITY_EDITOR
        //Debug.LogFormat("[Url = {0}]", url);
#endif
    }

    void Request<T>(string url, ServerResponseCallback<T> serverCallback, ClientResponseCallback<T> clientCallback, RequestBase request, RequestMethod? method = null, bool addRequestTimeout = true, bool sendRequestEvent = true)
    {
        if (!readyToRequest)
        {
            Debug.LogErrorFormat("Set Application Key!!");
            return;
        }

        SetRequestBase(request);

        Thread t;
        t = new Thread(() => ThreadedRequest(url, serverCallback, clientCallback, request, method));
        t.Start();

#if UNITY_EDITOR
        Debug.LogFormat("[Url = {0}]", url);
#endif
    }

    bool readyToRequest { get { return !string.IsNullOrEmpty(ClientData.applicationKey); } }

    void SetRequestBase(RequestBase request)
    {
        if (request == null)
            return;

        RequestCount++;
        ClientData.MergeWithRequestBase(request);
        request.SessionTime = (long)(Time.realtimeSinceStartup * 1000.0f);
        request.SessionOrder = instance.RequestCount;
    }

    void ThreadedRequest(string url, RequestBase request, RequestMethod? method = null)
    {
        DoThreadedRequest(url, request, method);
    }

    void ThreadedRequest<T>(string url, ServerResponseCallback<T> serverCallback, ClientResponseCallback<T> clientCallback, RequestBase request, RequestMethod? method = null)
    {
        string json = DoThreadedRequest(url, request, method);
        EnqueueAction(serverCallback, clientCallback, true, json);
    }

    string DoThreadedRequest(string url, RequestBase request, RequestMethod? method = null)
    {
        using (WebClient client = new WebClient())
        {
            client.Encoding = System.Text.Encoding.UTF8;

            string json = "{}";
            if (request == null || !method.HasValue || method != RequestMethod.POST)
            {
                json = client.DownloadString(url);
            }
            else if (request != null && method.HasValue && method == RequestMethod.POST)
            {
                client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                string json_request = JsonUtility.ToJson(request);
                json = client.UploadString(url, (method == RequestMethod.POST) ? "POST" : "GET", json_request);
            }

            return json;
        }
    }

    void EnqueueAction<T>(ServerResponseCallback<T> serverCallback, ClientResponseCallback<T> clientCallback, bool success, string json)
    {
        lock (queue)
        {
            queue.Enqueue(() =>
            {
#if UNITY_EDITOR
                Debug.LogFormat("Response [Json = {0}]", json);
#endif
                serverCallback(true, json, clientCallback);
            });
        }
    }

    void EnqueueAction(ResponseCallback callback, bool success, string json, long serverTimeInSeconds = 0, long expirationTime = 0)
    {
        lock (queue)
        {
            queue.Enqueue(() =>
            {
#if UNITY_EDITOR
                Debug.LogFormat("Response [Json = {0}]", json);
#endif
                callback(true, json);
            });
        }
    }
    #endregion

}
