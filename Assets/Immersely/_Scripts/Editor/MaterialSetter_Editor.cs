﻿
using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.SceneManagement;
namespace Immersely
{

    [CustomEditor(typeof(MaterialSetter))]
    public class MaterialSetter_Editor : Editor
    {
        //private int _choiceIndex;


        void OnEnable()
        {

        }

        public override void OnInspectorGUI()
        {

            serializedObject.Update();


            MaterialSetter myScript = (MaterialSetter)target;

            //Title
            EditorGUILayout.Space();

            GUILayout.Label(myScript.KeyName);

            EditorGUILayout.Space();
            GUILayout.Label(myScript.Key);
            EditorGUILayout.Space();
            //AdminPanelButton
            GUILayoutOption[] optionsAdminbutton = { GUILayout.MaxWidth(100), GUILayout.MinWidth(70), GUILayout.Height(30) };
            GUI.skin.button.wordWrap = true;
            Color oldColor = GUI.backgroundColor;

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            GUI.backgroundColor = Color.red;
            if (GUILayout.Button("Remove", optionsAdminbutton))
            {
                myScript.Clicked_Remove();

            }
            GUILayout.FlexibleSpace();

            GUI.backgroundColor = Color.yellow;
            if (GUILayout.Button("Edit", optionsAdminbutton))
            {
                myScript.Clicked_Edit();

            }

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUI.backgroundColor = oldColor;

            if (target != null && serializedObject != null)
                serializedObject.ApplyModifiedProperties();

            if (target != null)
                EditorUtility.SetDirty(target);

        }
    }
}

