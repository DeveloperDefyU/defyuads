﻿
using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.SceneManagement;
namespace Immersely
{
    [CustomEditor(typeof(Immersely_Container))]
    public class MaterialContainer_Editor : Editor
    {
        //private int _choiceIndex;
        SerializedProperty sourceTexture;
        SerializedProperty logoPosition;
        private GUIContent button_tex_CV90, button_tex_CCV90, button_tex_180;

        public Texture logoTexture;

        void OnEnable()
        {
            sourceTexture = serializedObject.FindProperty("SourceTexture");
            logoPosition = serializedObject.FindProperty("PositionLogo");
            button_tex_CV90 = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Immersely/_Textures/rotateArrow_CV.png", typeof(Texture)));
            button_tex_CCV90 = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Immersely/_Textures/rotateArrow_CCV.png", typeof(Texture)));
            button_tex_180 = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Immersely/_Textures/rotateArrow_180.png", typeof(Texture)));
            logoTexture = (Texture)AssetDatabase.LoadAssetAtPath("Assets/Immersely/_Textures/logo.png", typeof(Texture));
        }

        public override void OnInspectorGUI()
        {

            serializedObject.Update();
            sourceTexture = serializedObject.FindProperty("SourceTexture");
            logoPosition = serializedObject.FindProperty("PositionLogo");

            Immersely_Container myScript = (Immersely_Container)target;

            //Title
            EditorGUILayout.Space();
            // EditorGUILayout.LabelField("Material Maker", EditorStyles.boldLabel);
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label(logoTexture, GUILayout.Width(250), GUILayout.Height(100));
            GUILayout.FlexibleSpace();

            //AdminPanelButton
            GUILayoutOption[] optionsAdminbutton = { GUILayout.MaxWidth(100), GUILayout.MinWidth(70), GUILayout.Height(50) };
            GUI.skin.button.wordWrap = true;
            if (GUILayout.Button("Go to Admin Panel", optionsAdminbutton))
            {
                myScript.Clicked_AdminPanel();

            }

            GUILayout.EndHorizontal();
            //EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            EditorGUILayout.Space();
            //EditorGUILayout.LabelField("Choose Key");

            if (myScript.NoKeyFound)
            {

                //  EditorGUILayout.HelpBox("No placement keys could be found, place make sure \n · to set them up at the admin panel \n · check you internet connection \n · the Defyu_AdsManager is present in the scene \n · you introduced the app key", MessageType.Error);
                EditorGUILayout.HelpBox(myScript.ErrorMessage, MessageType.Error);

                CancelButton(myScript);

                if (target != null && serializedObject != null)
                    serializedObject.ApplyModifiedProperties();

                if (target != null)
                    EditorUtility.SetDirty(target);


                return;
            }
            //Key dropdown
            string[] _choices = myScript.KeyOptions;
            int choice = EditorGUILayout.Popup(myScript.ChoiceIndex, _choices, GUILayout.Width(200));
            if (choice != myScript.ChoiceIndex)
            {
                myScript.OnKeySelected(choice);
            }

            //EditorGUILayout.Space();
            EditorGUILayout.LabelField(myScript.CurrentDescription);
            EditorGUILayout.Space();
            EditorGUILayout.Space();

            //Material options
            if (myScript.KeySelected)
            {
                if (myScript.HasMaterial)
                {
                    MaterialOptions(myScript);

                    if (myScript.MaterialRegistered)
                    {
                        RegisteredButtons(myScript);
                    }
                    else
                    {
                        MakeMaterialButtons(myScript);

                    }
                }
                else
                {
                    ShowEmptyButtons(myScript);
                }



            }

            CancelButton(myScript);

            if (target != null && serializedObject != null)
                serializedObject.ApplyModifiedProperties();

            if (target != null)
                EditorUtility.SetDirty(target);

        }

        private void MaterialOptions(Immersely_Container myScript)
        {


            GUIStyle gsAlterQuest = new GUIStyle();
            gsAlterQuest.normal.background = MakeTex(600, 1, new Color(0.5f, 0.5f, 0.5f, 1f));

            GUILayoutOption[] optionsLabel = { GUILayout.MinWidth(150) };


            GUILayout.BeginVertical(gsAlterQuest);

            EditorGUILayout.Space();

            // source texture
            GUILayout.BeginHorizontal();
            GUILayout.Label("Source Texture", EditorStyles.boldLabel, optionsLabel);
            EditorGUILayout.PropertyField(sourceTexture, GUIContent.none);


            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            EditorGUILayout.Space();

            //Position rect
            GUILayout.BeginHorizontal();
            GUILayout.Label("Frame Position", EditorStyles.boldLabel, optionsLabel);
            GUILayout.FlexibleSpace();
            GUILayoutOption[] optionsRect = { GUILayout.MaxWidth(150), GUILayout.MinWidth(100) };
            EditorGUILayout.PropertyField(logoPosition, GUIContent.none, optionsRect);
            GUILayout.FlexibleSpace();

            //UV buttons
            GUILayoutOption[] optionsUVbutton = { GUILayout.MaxWidth(70), GUILayout.MinWidth(50), GUILayout.Height(35) };
            GUI.skin.button.wordWrap = true;
            if (myScript.Catching)
            {
                if (GUILayout.Button("Hide Handles", optionsUVbutton))
                {
                    myScript.Clicked_UV_Finished();

                }
            }
            else
            {
                if (GUILayout.Button("CATCH UVs", optionsUVbutton))
                {
                    myScript.Clicked_CatchUV();

                }
            }
            GUI.skin.button.wordWrap = false;
            GUILayout.EndHorizontal();

            EditorGUILayout.Space();

            //Rotation
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("Rotate", EditorStyles.boldLabel, optionsLabel);
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();


            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button(button_tex_CCV90, GUILayout.Width(50), GUILayout.Height(50)))
            {
                myScript.Rotate90CCV();
            }
            if (GUILayout.Button(button_tex_180, GUILayout.Width(50), GUILayout.Height(50)))
            {
                myScript.Rotate180();
            }

            if (GUILayout.Button(button_tex_CV90, GUILayout.Width(50), GUILayout.Height(50)))
            {
                myScript.Rotate90CV();
            }
            GUILayout.FlexibleSpace();


            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            EditorGUILayout.Space();

            GUILayout.EndVertical();



        }

        private void RegisteredButtons(Immersely_Container myScript)
        {

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            GUILayoutOption[] options = { GUILayout.MaxWidth(100.0f), GUILayout.MinWidth(10.0f), GUILayout.Height(30) };
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            Color oldColor = GUI.backgroundColor;
            GUI.backgroundColor = Color.red;
            if (GUILayout.Button("OVERWRITE", options))
            {
                myScript.Clicked_OverWrite();

            }
            GUILayout.FlexibleSpace();
            GUI.backgroundColor = Color.green;
            if (GUILayout.Button("APPLY", options))
            {
                myScript.Clicked_Apply();

            }
            GUI.backgroundColor = oldColor;
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            EditorGUILayout.HelpBox("CAUTION: Overwriting destroys previous material and applies to all assets using this key", MessageType.Warning);



            EditorGUILayout.Space();
            EditorGUILayout.Space();


        }

        private void MakeMaterialButtons(Immersely_Container myScript)
        {
            GUILayoutOption[] options = { GUILayout.MaxWidth(100.0f), GUILayout.MinWidth(10.0f), GUILayout.Height(30) };

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            Color oldColor = GUI.backgroundColor;
            GUI.backgroundColor = Color.green;

            if (GUILayout.Button("Make Material", options))
            {
                myScript.Clicked_MakeMaterial();

            }
            GUI.backgroundColor = oldColor;

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }

        private void ShowEmptyButtons(Immersely_Container myScript)
        {
            GUILayoutOption[] options = { GUILayout.MaxWidth(100.0f), GUILayout.MinWidth(10.0f), GUILayout.Height(30) };

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            Color oldColor = GUI.backgroundColor;
            GUI.backgroundColor = Color.green;

            if (GUILayout.Button("APPLY", options))
            {
                myScript.Clicked_ApplyEmpty();

            }
            GUI.backgroundColor = oldColor;

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }

        private Texture2D MakeTex(int width, int height, Color col)
        {
            Color[] pix = new Color[width * height];

            for (int i = 0; i < pix.Length; i++)
                pix[i] = col;

            Texture2D result = new Texture2D(width, height);
            result.SetPixels(pix);
            result.Apply();

            return result;
        }

        private void CancelButton(Immersely_Container myScript)
        {
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            Color orgColor = GUI.backgroundColor;
            GUI.backgroundColor = Color.yellow;
            if (GUILayout.Button("Cancel"))
            {
                myScript.Clicked_Cancel();

            }
            GUI.backgroundColor = orgColor;


            GUILayout.EndHorizontal();
        }
    }
}


