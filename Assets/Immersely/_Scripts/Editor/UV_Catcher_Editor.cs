﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
namespace Immersely
{
    [CustomEditor(typeof(UV_Catcher))]
    public class UV_Catcher_Editor : Editor
    {

        UV_Catcher catcher;

        void Start()
        {
            catcher = (UV_Catcher)target;
        }

        void OnSceneGUI()
        {

            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));

            catcher = (UV_Catcher)target;

            SetHandles(catcher);

            DoRaycast(catcher);

        }

        private void DoRaycast(UV_Catcher catcher)
        {
            RaycastHit hit;
            Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

            if (!Physics.Raycast(ray, out hit))
                return;

            if (catcher._nPointsSet > 0 && catcher._nPointsSet < 4)
            {
                Handles.color = Color.white;
                Handles.DrawLine(catcher.LastPoint, hit.point);
            }

            if (!(Event.current.type == EventType.MouseDown && Event.current.button == 0))
                return;

            Renderer rend = hit.transform.GetComponent<Renderer>();
            if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null)
                return;

            Texture tex = rend.sharedMaterial.mainTexture as Texture;
            Vector2 pixelUV = hit.textureCoord;
            pixelUV.x *= tex.width;
            pixelUV.y *= tex.height;

            catcher.PointSet(pixelUV, hit.point);
        }

        void SetHandles(UV_Catcher catcher)
        {
            for (int i = 1; i < catcher._nPointsSet; i++)
            {
                Handles.color = Color.blue;
                Handles.DrawLine(catcher._worldPoints[i - 1], catcher._worldPoints[i]);
            }

            for (int i = 0; i < catcher._nPointsSet; i++)
            {
                Handles.color = Color.blue;
                MoveHandle(catcher, i);
            }

            if (catcher._nPointsSet == 4)
            {
                Handles.DrawLine(catcher._worldPoints[3], catcher._worldPoints[0]);
            }

        }

        void MoveHandle(UV_Catcher catcher, int i)
        {

            EditorGUI.BeginChangeCheck();
            Vector3 snap = Vector3.one * 0.5f;
            float size = Vector3.Distance(catcher._worldPoints[i], Camera.current.transform.position) * 0.05f;
            //Vector3 newTargetPosition = Handles.FreeMoveHandle(catcher._worldPoints[i], Quaternion.identity, size, snap, Handles.SphereHandleCap);
            Handles.FreeMoveHandle(catcher._worldPoints[i], Quaternion.identity, size, snap, Handles.SphereHandleCap);

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(catcher, "Change Look At Target Position");

                RaycastHit hit;
                Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
                if (!Physics.Raycast(ray, out hit))
                    return;


                Renderer rend = hit.transform.GetComponent<Renderer>();
                //MeshCollider meshCollider = hit.collider as MeshCollider;

                if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null)
                    return;



                Texture tex = rend.material.mainTexture as Texture;
                Vector2 pixelUV = hit.textureCoord;
                pixelUV.x *= tex.width;
                pixelUV.y *= tex.height;

                catcher.PointSet(pixelUV, hit.point, i);

                //catcher.Update();
            }
        }

    }
}