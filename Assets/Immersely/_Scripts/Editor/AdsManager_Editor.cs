﻿
using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.SceneManagement;
namespace Immersely
{

    [CustomEditor(typeof(Immersely_AdsManager))]
    public class AdsManager_Editor : Editor
    {
        public Texture logoTexture;
        SerializedProperty applicationId;
        bool realod;

        private void OnEnable()
        {
            applicationId = serializedObject.FindProperty("ApplicationKey");
            //realod = serializedObject.FindProperty("ReloadOnCompiling");
            logoTexture = (Texture)AssetDatabase.LoadAssetAtPath("Assets/Immersely/_Textures/logo.png", typeof(Texture));
        }
       
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            return;

            serializedObject.Update();


            Immersely_AdsManager myScript = (Immersely_AdsManager)target;

            //Title
            EditorGUILayout.Space();

            GUILayout.Label("Application Key");
           
            EditorGUILayout.PropertyField(applicationId, GUIContent.none);

            if (myScript.NoKeyFound)
            {

                EditorGUILayout.HelpBox("Please enter a valid Application Key", MessageType.Error);
               
            }

            EditorGUILayout.Space();

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label(logoTexture, GUILayout.Width(250), GUILayout.Height(100));
            GUILayout.FlexibleSpace();

            //AdminPanelButton
            GUILayoutOption[] optionsAdminbutton = { GUILayout.MaxWidth(100), GUILayout.MinWidth(70), GUILayout.Height(50) };
            GUI.skin.button.wordWrap = true;
            if (GUILayout.Button("Go to Admin Panel", optionsAdminbutton))
            {
                myScript.Clicked_AdminPanel();

            }
            GUILayout.EndHorizontal();
            EditorGUILayout.Space();
            

            realod = EditorGUILayout.ToggleLeft("Reset server connection when compiling", realod);
            myScript.ReloadOnCompiling = realod;

            EditorGUILayout.Space();

            
            if (target != null && serializedObject != null)
                serializedObject.ApplyModifiedProperties();

            if (target != null)
                EditorUtility.SetDirty(target);

        }
    }
}

