﻿
using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Immersely
{
    [ExecuteInEditMode]
    public class Immersely_AdsManager : MonoBehaviour
    {

        #region singleton
        private static Immersely_AdsManager _instance;
        public static Immersely_AdsManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<Immersely_AdsManager>();

                    if (_instance == null)
                    {
                        Debug.LogError("Please add the Defyu_AdsManager to the scene!");

                    }
                }

                return _instance;
            }
        }

       




        #endregion

        #region fields
        public string ApplicationKey;
        [HideInInspector]
        public static string AdminUrl = "http://lasse.defyu.com/publisher/applications";

        private Dictionary<string, AssetObjectContent> Contents;

        //TODO check on server if Key valid
        public bool NoKeyFound { get {return !(ApplicationKey.Length > 0); } }

        /*
         * Handy when working in the editor, but causes problems when enter in play mode
         */
        public bool ReloadOnCompiling;

       
        #endregion

        #region delegates
        public delegate void OnInitializationFinished();
        public static OnInitializationFinished InitializationFinishedDelegate;
        #endregion

        #region Initialize
#if UNITY_EDITOR
        [UnityEditor.Callbacks.DidReloadScripts]
        private static void OnScriptsReloaded()
        {
            if (!Application.isPlaying && Instance.ReloadOnCompiling)
                Instance.Initialize();
        }
#endif

        void Start()
        {
            Initialize();

        }

        public static void Reset()
        {
            Instance.Initialize();
        }

        private void Initialize()
        {           

            WebApiClient.Initialize();
            ClientData.applicationKey = ApplicationKey;

#if UNITY_EDITOR
           // Ask_SessionStart();
            AskForPlacementsEditor();
#else
        Ask_SessionStart();
#endif
        }

        void OnApplicationQuit()
        {
            Ask_SessionEnd();
        }




        #endregion

        #region GetAssets

        private void AskForPlacements()
        {
            Ask_AllPlacements();
        }

        private void AskForPlacementsEditor()
        {
            // FakePlacements();
           
            Ask_AllPlacementsEditor();
        }

        private void SetAllPlacements(AllPlacementsAnswer answer)
        {
            
            foreach (var aa in answer.AssetAnswers)
            {
                var content = ScriptableObject.CreateInstance("AssetObjectContent") as AssetObjectContent;
                content.SetAssetObjectContent(aa);
                content.Contracted = true;
                if (Contents.ContainsKey(aa.Id))
                {                   
                    Contents[aa.Id] = content;
                }
                   
                else
                    Contents.Add(aa.Id, content);
                if (Application.isPlaying && content.NeedsDownload)
                {
                    DownloadManager.AddContent(content);
                }
            }
          
              //if (InitializationFinishedDelegate != null)
              //    InitializationFinishedDelegate();

        }



        private void SetAllPlacementsEditor(AllPlacementsAnswer answer)
        {
            Contents = new Dictionary<string, AssetObjectContent>();


            foreach (var aa in answer.AssetAnswers)
            {

                var content = ScriptableObject.CreateInstance("AssetObjectContent") as AssetObjectContent;
                content.SetAssetObjectContent(aa);
                if (Contents.ContainsKey(aa.Id))
                {
                    Contents[aa.Id] = content;
                }
                else
                {
                    Contents.Add(aa.Id, content);
                }


                if (Application.isPlaying && content.NeedsDownload)
                {
                    DownloadManager.AddContent(content);
                }


            }
          
            if (InitializationFinishedDelegate != null)
                InitializationFinishedDelegate();
            

        }



        #endregion

        #region server comunication assets

        public void Clicked_AdminPanel()
        {
            Application.OpenURL(Immersely_AdsManager.AdminUrl);
        }

        private void Ask_SessionStart()
        {

            WebApiClient.Request_SessionStart((bool success, SessionEventResponse sessionStartData) =>
            {
                if (success)
                {

                    Callback_SessionStart(sessionStartData);
                }
                else
                {
                    Debug.LogError("Could not start session");
                }

            });

        }

        private void Ask_SessionEnd()
        {
            WebApiClient.Request_SessionEnd();
        }

        private void Callback_SessionStart(SessionEventResponse entity)
        {
              AskForPlacementsEditor();
        }

        private void Ask_AllPlacements()
        {
            WebApiClient.Request_AllPlacements(Callback_AllPlacements);
        }

        private void Callback_AllPlacements(bool success, AllPlacementsAnswer entity)
        {
           SetAllPlacements(entity);
        }

        private void Ask_AllPlacementsEditor()
        {
            WebApiClient.Request_AllPlacementsForEditor(Callback_AllPlacementsEditor);
        }

        private void Callback_AllPlacementsEditor(bool success, AllPlacementsAnswer entity)
        {
            SetAllPlacementsEditor(entity);
           
           if (Application.isPlaying)
            {               
                Ask_AllPlacements();
                return;
            }
#if UNITY_EDITOR
           
            var materialContainers = FindObjectsOfType<Immersely_Container>();
            foreach (var mc in materialContainers)
            {
                mc.Initialize();
            }            
#endif
        }

        #endregion

        #region getter & setter
        public static bool IsKeyEnabled(string key)
        {
            return Instance.Contents.ContainsKey(key);
        }

        internal static string[] GetMaterialKeys()
        {
            return Instance._GetMaterialKeys();
        }


        private string[] _GetMaterialKeys()
        {
            List<string> keys = new List<string>();

            foreach (var item in Contents)
            {
                if (item.Value.ContentType == ContentTypes.Texture)
                    keys.Add(item.Key);
            }

            return keys.ToArray();
        }

        internal static AssetObjectContent[] GetMaterialAssets()
        {
            if (Instance == null)
            {
                return new AssetObjectContent[0];
            }
            return Instance._GetMaterialAssets();
        }

        private AssetObjectContent[] _GetMaterialAssets()
        {
            List<AssetObjectContent> keys = new List<AssetObjectContent>();

            if (Contents == null || Contents.Count == 0)
            {
                Debug.LogError(" NO CONTENTS");
                return keys.ToArray();
            }

            foreach (var item in Contents)
            {
                if (item.Value.ContentType == ContentTypes.Texture)
                    keys.Add(item.Value);
            }

            return keys.ToArray();
        }

        internal static AssetObjectContent[] GetAllAssets()
        {
            if (Instance == null)
            {
                return new AssetObjectContent[0];
            }
            return Instance._GetAllAssets();
        }

        private AssetObjectContent[] _GetAllAssets()
        {
            List<AssetObjectContent> keys = new List<AssetObjectContent>();

            if (Contents == null || Contents.Count == 0)
            {
                Debug.LogError(" NO CONTENTS");
                return keys.ToArray();
            }

            foreach (var item in Contents)
            {
                if (item.Value.HasKey)
                    keys.Add(item.Value);
            }

            return keys.ToArray();
        }


        internal bool IsEmpty(string key)
        {
            if (Contents== null || !Contents.ContainsKey(key))
            {
                Debug.LogError("key: " + key + "not found in Contents, check material list prefab");
                return true;
            }
            return Contents[key].IsEmpty;
        }

        internal bool HasMaterial(string key)
        {
            return Contents[key].HasMaterial;
        }

        internal static bool IsContracted(string key)
        {
            if (!Instance.Contents.ContainsKey(key))
                return false;

            return Instance.Contents[key].Contracted;
        }

        #endregion

        #region fake answer
        private void FakePlacements()
        {
            AllPlacementsAnswer entity = new AllPlacementsAnswer();
            entity.AssetAnswers = new List<AssetAnswer>();
            entity.AssetAnswers.Add(new AssetAnswer()
            {
                Id = "a1",
                Url = "http://www.stickpng.com/assets/images/580b57fcd9996e24bc43c4f8.png",
                Name = "Building",
                Description = "jhsgdjhsgd",
                Type = ContentTypes.Texture,
            });

            entity.AssetAnswers.Add(new AssetAnswer()
            {
                Id = "a2",
                Url = "http://pngimg.com/uploads/adidas/adidas_PNG21.png",
                Name = "Tree",
                Description = "asfsafdsf",
                Type = ContentTypes.Texture,
            });

            entity.AssetAnswers.Add(new AssetAnswer()
            {
                Id = "a3",
                Url = "",
                Name = "Player",
                Description = "jhsgdjhsgd",
                Type = ContentTypes.Empty,
            });

            Callback_AllPlacementsEditor(true, entity);

        }

#endregion


    }
}
