﻿#if (UNITY_EDITOR)
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Immersely
{
    [ExecuteInEditMode]
    public class UV_Catcher : MonoBehaviour
    {

        #region fields
        public int _nPointsSet;
        private Vector2[] _points = new Vector2[4];
        public Vector3[] _worldPoints = new Vector3[4];
        public Vector3 LastPoint { get { return _worldPoints[_nPointsSet - 1]; } }
        #endregion

        #region methods

        void Start()
        {
            GUIUtility.hotControl = 0;
        }

        public void SetHandlesFromUV(Vector2[] points, Vector3[] worldPoints)
        {
            _worldPoints = worldPoints;
            _points = points;
            _nPointsSet = 4;
        }

        public void PointSet(Vector2 point, Vector3 worldPoint)
        {
            if (_nPointsSet >= 4)
            {
                return;
            }
            _worldPoints[_nPointsSet] = worldPoint;
            _points[_nPointsSet] = point;
            _nPointsSet++;

            if (_nPointsSet >= 4)
            {
                Finished();
            }
        }

        public void PointSet(Vector2 point, Vector3 worldPoint, int i)
        {

            _worldPoints[i] = worldPoint;
            _points[i] = point;



            Finished();

        }

        private void Finished()
        {

            var minX = Mathf.Infinity;
            var maxX = 0f;

            var minY = Mathf.Infinity;
            var maxY = 0f;

            foreach (var point in _points)
            {
                minX = Mathf.Min(minX, point.x);
                maxX = Mathf.Max(maxX, point.x);

                minY = Mathf.Min(minY, point.y);
                maxY = Mathf.Max(maxY, point.y);
            }
            Rect rectPosistion = new Rect(minX, minY, (maxX - minX), (maxY - minY));

            // Rect rectPosistion = new Rect(_points[0].x, _points[0].y, (_points[3].x - _points[0].x), (_points[2].y - _points[0].y));

            var container = GetComponent<Immersely_Container>();
            container.SetUVs(rectPosistion);


        }
        #endregion


    }
}
#endif
