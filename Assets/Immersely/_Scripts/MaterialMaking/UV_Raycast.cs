﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UV_Raycast : MonoBehaviour {

    private RaycastHit hit;
    Vector3 direction;
    Vector3 startPoint;
    float distance;

    public LayerMask mask;

    Vector2[,] uvCoordinates;

    int xResolution = 128;
    int yResolution = 128;

    float xStep;
    float yStep;

    public Renderer Target;
	// Use this for initialization
	void Start () {
        uvCoordinates = new Vector2[xResolution, yResolution];

        xStep = transform.localScale.x / xResolution ;
        yStep = transform.localScale.y / yResolution ;

        distance = transform.localScale.z ;
        AllCasts();
        TestTexture();

    }
	
	// Update is called once per frame
	void Update () {
        
    }

    void AllCasts()
    {
        startPoint = transform.position - (transform.localScale *0.5f);
        direction = transform.forward;
        var nullPoint = startPoint;
        for (int i = 0; i < xResolution; i++)
        {
            for (int j = 0; j < yResolution; j++)
            {
                startPoint.x = nullPoint.x + xStep * i;
                startPoint.y = nullPoint.y + yStep * j;
                OneCast(i, j);
            }
        }
    }

    private void OneCast(int i, int j)
    {
        if (Physics.Raycast(startPoint, direction, out hit, distance, mask))
        {
            Debug.DrawRay(startPoint, direction.normalized * distance, Color.yellow, 5);
            uvCoordinates[i, j] = hit.textureCoord ;
        }
    }

    private void TestTexture()
    {
       StartCoroutine( AddLogo(Target.material.mainTexture as Texture2D));
    }

    private IEnumerator AddLogo(Texture2D SourceTexture)
    {

        int width = SourceTexture.width;
        int height = SourceTexture.height;

        for (int i = 0; i < xResolution; i++)
        {
            for (int j = 0; j < yResolution; j++)
            {
                int x = Mathf.RoundToInt(uvCoordinates[i, j].x * width);
                int y = Mathf.RoundToInt(uvCoordinates[i, j].y * height);
                SourceTexture.SetPixel(x,y,Color.red);
              
            }
        }
        SourceTexture.Apply();

         yield return null;



    }
}
