﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Immersely
{
    [System.Serializable]
    public struct MaterialContainerStruct
    {

        public string Key;
        public Texture2D SourceTexture;
        public Rect PositionLogo;
        public LogoRotation RotationLogo;
        public Texture StencilTexture;
        public int StencilKey;
    }

    [System.Serializable]
    public enum LogoRotation
    {
        NONE,
        CV_90,
        FLIP,
        CCV_90,
    }
    

}
