﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Immersely
{
    public class MaterialSetter : MonoBehaviour
    {

        #region fields
        public string Key;
        public string KeyName;
        [SerializeField]
        private Material[] _orgMaterials;

        [SerializeField]
        Texture _stencilTexture;
        int _stencilKey;

        private Renderer _renderer;

        private bool IsEmpty { get { return Immersely_AdsManager.Instance.IsEmpty(Key); } }

        private bool _setUp;
        #endregion

        #region Initialisation & destruction events
        private void OnEnable()
        {
            MaterialManager.MaterialFinishedDelegate += SetMaterial;
            MaterialManager.MaterialPreparedDelegate += OnMaterialPrepared;
#if UNITY_EDITOR
            _renderer = GetComponent<Renderer>();
            _orgMaterials = _renderer.materials;
#endif

        }

        private void OnDisable()
        {
            MaterialManager.MaterialFinishedDelegate -= SetMaterial;
            MaterialManager.MaterialPreparedDelegate -= OnMaterialPrepared;
        }



        private void OnDestroy()
        {
#if UNITY_EDITOR
            CleanUp(true);
#endif
        }
#if UNITY_EDITOR
        public void Clicked_Remove()
        {
            CleanUp();
        }

        public void Clicked_Edit()
        {
            gameObject.AddComponent<Immersely_Container>();
            CleanUp();

        }
#endif
        #endregion

        #region set material


        private void OnMaterialPrepared()
        {
            if (_setUp)
                return;

            SetUpMaterials();
            _setUp = true;
        }

        private void SetMaterial(string key)
        {
            if (IsEmpty || key != Key)
                return;

            Debug.Log("Set material");

            if (_renderer == null)
                _renderer = GetComponent<Renderer>();


            _renderer.materials[0].mainTexture = MaterialManager.GetTexture(Key);


            // DefyuAds_Analytics.OnMaterialMade(Key);
        }



        #endregion

        #region set up & clean up


        internal void SetUp(string key, string currentKeyName/*, Material[] originalMaterial, VisibilityChecker checker*/)
        {
            Key = key;
            KeyName = currentKeyName;

        }

        private void CleanUp(bool alreadyDestroyed = false)
        {
            if (_orgMaterials != null && _orgMaterials.Length > 0)
                GetComponent<Renderer>().materials = _orgMaterials;

            if (!alreadyDestroyed)
                DestroyImmediate(this);
        }

        private void SetUpMaterials()
        {
            _renderer = GetComponent<Renderer>();
            _orgMaterials = _renderer.materials;


            Material[] mats = new Material[_orgMaterials.Length + 1];
            for (int i = 0; i < _orgMaterials.Length; i++)
            {
                mats[i] = _orgMaterials[i];
            }

            // mats[0].mainTexture = MaterialManager.GetTexture(Key);

            mats[_orgMaterials.Length] = new Material(Shader.Find("Defyu/StencilWrite"));
            mats[_orgMaterials.Length].SetInt("_StencilMask", MaterialManager.GetStencilKey(Key));
            Texture stencilUV_texture = MaterialManager.GetStencilTexture(Key);
            if (stencilUV_texture == null)
                mats[_orgMaterials.Length].SetInt("_WriteAll", 1);
            else
            {
                mats[_orgMaterials.Length].SetInt("_WriteAll", 0);
                mats[_orgMaterials.Length].SetTexture("_MainTex", MaterialManager.GetStencilTexture(Key));
            }


            _renderer.materials = mats;
        }

        #endregion
    }
}
