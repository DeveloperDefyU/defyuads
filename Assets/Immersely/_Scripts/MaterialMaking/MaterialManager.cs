﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
namespace Immersely
{
    public class MaterialManager : MonoBehaviour
    {

        #region singleton
        private static MaterialManager _instance;
        public static MaterialManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<MaterialManager>();

                    if (_instance == null)
                    {
                        GameObject go = new GameObject();
                        go.name = "MaterialManager";
                        _instance = go.AddComponent<MaterialManager>();
                        DontDestroyOnLoad(go);
                    }
                }

                return _instance;
            }
        }
        #endregion

        public Dictionary<string, Texture> FinishedTextures = new Dictionary<string, Texture>();
        // public Dictionary<string, Texture> StencilTextures = new Dictionary<string, Texture>();

        public Texture2D[] StencilTest = new Texture2D[3];
        private int kkCounter;

        List<MaterialMaker> TodoList = new List<MaterialMaker>();

        List<AssetObjectContent> AdsContent = new List<AssetObjectContent>();
        public List<MaterialContainerStruct> Containers = new List<MaterialContainerStruct>();

        public MaterialList MaterialListPrefab;
        private MaterialList _containers;
        private static bool _working;

        public delegate void OnMaterialFinished(string key);
        public static OnMaterialFinished MaterialFinishedDelegate;

        public delegate void OnMaterialPrepared();
        public static OnMaterialPrepared MaterialPreparedDelegate;

        public static int NumberOfContainers { get { return Instance.Containers.Count; } }

        public static List<string> MaterialKeys
        {
            get
            {
                List<string> temp = new List<string>();
                foreach (var item in Instance.Containers)
                {
                    temp.Add(item.Key);
                }
                return temp;
            }
        }

        public static Dictionary<string, int> StencilKeys
        {
            get
            {
                Dictionary<string, int> temp = new Dictionary<string, int>();
                foreach (var item in Instance.Containers)
                {
                    temp.Add(item.Key, item.StencilKey);
                }
                return temp;
            }
        }

        private void OnEnable()
        {
            Immersely_AdsManager.InitializationFinishedDelegate += Initialize;
        }

        private void OnDisable()
        {
            Immersely_AdsManager.InitializationFinishedDelegate -= Initialize;
        }

        private void Initialize()
        {
            Debug.Log("init MM");
            _containers = Instantiate(MaterialListPrefab);
           
            Containers = new List<MaterialContainerStruct>();
            foreach (var item in _containers.AllMaterials)
            {
                AddContainer(item);
            }
            
            if (MaterialPreparedDelegate != null)
                MaterialPreparedDelegate();
        }

        private void Update()
        {
            //Debug.Log("is working: " + _working + " List count: " + TodoList.Count);
            if (!_working && TodoList.Count > 0)
            {
                MakeNextMaterial();
            }
        }


        #region getters
        public static Texture GetTexture(string key)
        {
            if (Instance.FinishedTextures == null)
            {
                Debug.LogError("FinishedTextures has not been initialized");
            }
            if (!Instance.FinishedTextures.ContainsKey(key))
            {
                Debug.LogError("FinishedTextures does not contain key: " + key);
            }
            return Instance.FinishedTextures[key];
        }

        public static Texture GetStencilTexture(string key)
        {
            if (Immersely_AdsManager.Instance.IsEmpty(key))
            {
                return null;
            }

            var container = Instance.Containers.First(x => x.Key == key);
            if (container.StencilTexture == null)
            {
                Debug.LogError("StencilTextures has not been initialized");
                return null;
            }
            return container.StencilTexture;

        }

        public static int GetStencilKey(string key)
        {
            var container = Instance.Containers.FirstOrDefault(x => x.Key == key);

            return container.StencilKey;


        }

        public static MaterialAnswer TextureStatus(string key)
        {
            if (Instance.FinishedTextures.ContainsKey(key))
                return MaterialAnswer.Ready;

            if (!Immersely_AdsManager.IsKeyEnabled(key))
                return MaterialAnswer.NoAd;

            if (!Instance.TodoList.Any(x => x.Key == key))
                return MaterialAnswer.Processing;

            return MaterialAnswer.AdButNoMaterial;

        }
        #endregion

        #region List management
        internal static void AddContent(AssetObjectContent assetObjectContent)
        {
            Instance._AddContent(assetObjectContent);
        }

        private void _AddContent(AssetObjectContent assetObjectContent)
        {

            if (AdsContent == null)
                AdsContent = new List<AssetObjectContent>();

            if (AdsContent.Any(x => x.Key == assetObjectContent.Key))
            {
                AdsContent.Remove(AdsContent.First(x => x.Key == assetObjectContent.Key));
                AdsContent.Add(assetObjectContent);

                if (TodoList.Any(x => x.Key == assetObjectContent.Key))
                {
                    TodoList.Remove(TodoList.First(x => x.Key == assetObjectContent.Key));
                }
                if(Containers.Any(x => x.Key == assetObjectContent.Key))
                {
                    AddToList(assetObjectContent, Containers.First(x => x.Key == assetObjectContent.Key));
                }
                return;
            }
                

            AdsContent.Add(assetObjectContent);

            if (!FinishedTextures.ContainsKey(assetObjectContent.Key) && !TodoList.Any(x => x.Key == assetObjectContent.Key) && Containers.Any(x => x.Key == assetObjectContent.Key))
                AddToList(assetObjectContent, Containers.First(x => x.Key == assetObjectContent.Key));

        }

        internal static void AddContainer(MaterialContainerStruct container)
        {
            Instance._AddContainer(container);
        }

        private void _AddContainer(MaterialContainerStruct container)
        {
            if (Containers == null)
                Containers = new List<MaterialContainerStruct>();

            if (Containers.Any(x => x.Key == container.Key))
                return;

            Containers.Add(container);

            if (!FinishedTextures.ContainsKey(container.Key) && !TodoList.Any(x => x.Key == container.Key) && AdsContent.Any(x => x.Key == container.Key))
                AddToList(AdsContent.First(x => x.Key == container.Key), container);

        }

        private void AddToList(AssetObjectContent assetObjectContent, MaterialContainerStruct container)
        {

            TodoList.Add(new MaterialMaker(assetObjectContent, container));
        }

        #endregion

        #region make materials
        private void MakeNextMaterial()
        {

            _working = true;
            MakeMaterial(TodoList[0]);

        }

        private void MakeMaterial(MaterialMaker materialMaker)
        {
            materialMaker.GetCombinedTexture((bool success, Texture combinedTexture) =>
            {
                if (success)
                {
                    MaterialFinished(combinedTexture, materialMaker);
                }

            });
        }

        private void MaterialFinished(Texture combinedTexture, MaterialMaker maker)
        {
            _working = false;
            if (!FinishedTextures.ContainsKey(maker.Key))
                FinishedTextures.Add(maker.Key, combinedTexture);
            else
                FinishedTextures[maker.Key] = combinedTexture;


            TodoList.Remove(maker);

            if (MaterialFinishedDelegate != null)
                MaterialFinishedDelegate(maker.Key);
        }

        internal static int GetStencilKeyEditor()
        {
            throw new NotImplementedException();
        }

        #endregion



    }

    public enum MaterialAnswer
    {
        NoAd,
        Ready,
        Processing,
        AdButNoMaterial,
    }

}