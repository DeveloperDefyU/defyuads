﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
namespace Immersely
{
    public class MaterialList : MonoBehaviour
    {

        public List<MaterialContainerStruct> AllMaterials = new List<MaterialContainerStruct>();


        public void AddMaterial(MaterialContainerStruct material)
        {
            if (AllMaterials == null)
                AllMaterials = new List<MaterialContainerStruct>();

            AllMaterials.Add(material);
        }

        internal bool HasKey(string key)
        {
            if (AllMaterials == null)
                AllMaterials = new List<MaterialContainerStruct>();
            return AllMaterials.Any(x => x.Key == key);
        }

        public MaterialContainerStruct GetMaterial(string key)
        {
            if (AllMaterials == null)
                AllMaterials = new List<MaterialContainerStruct>();

            return AllMaterials.FirstOrDefault(x => x.Key == key);


        }

        internal void SubsitituteMaterial(MaterialContainerStruct materialContainerStruct)
        {
            AllMaterials.RemoveAll((x => x.Key == materialContainerStruct.Key));

            AddMaterial(materialContainerStruct);
        }

        internal int StencilIndexOf(string key)
        {
            if (!HasKey(key))
                return (AllMaterials.Count + 1) * 10;

            var index = AllMaterials.IndexOf(AllMaterials.First(x => x.Key == key));
            return (index + 1) * 10;
        }
    }
}
