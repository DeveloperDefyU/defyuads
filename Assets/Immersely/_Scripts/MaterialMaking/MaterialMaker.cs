﻿
using System;
using System.Collections;
using UnityEngine;

#if (UNITY_EDITOR)
using UnityEditor;
#endif
namespace Immersely
{
    public class MaterialMaker
    {

        #region public fields & properties
        public string Key { get { return _key; } }
        public delegate void MadeMaterialCallback<T>(bool success, T texture);
        #endregion

        #region private fields
        private ComputeShader _shader;
        private RenderTexture _resultingRenderTexture;
        private Vector4 _offset;
        private string _key;
        private Texture2D _sourceTexture;
        private Rect _positionLogo;
        private LogoRotation _rotationLogo;
        private Texture2D _logoTexture;
        private Texture2D _logoTextureRotated;
        private bool _rotated;
        #endregion

        #region Constructors

        public MaterialMaker(AssetObjectContent assetObjectContent, MaterialContainerStruct container)
        {
            _key = assetObjectContent.Key;
            _sourceTexture = container.SourceTexture;
            _positionLogo = container.PositionLogo;
            _logoTexture = assetObjectContent.Texture;
            _rotationLogo = container.RotationLogo;
        }

        public MaterialMaker(Texture2D logoTexture, MaterialContainerStruct container)
        {
            _key = container.Key;
            _sourceTexture = container.SourceTexture;
            _positionLogo = container.PositionLogo;
            _rotationLogo = container.RotationLogo;
            _logoTexture = logoTexture;
        }

        #endregion

        #region public methods

        public void DoRotation(MadeMaterialCallback<Texture2D> callback)
        {
            Rotate(callback);
        }

        public void DoRotation()
        {
            Rotate();
        }

        public void GetCombinedTexture(MadeMaterialCallback<Texture> callback, bool preview = false)
        {
            if (!preview)
                Rotate();
            else
                _logoTextureRotated = _logoTexture;

            if (SystemInfo.graphicsShaderLevel >= 45)
                RunShader(callback);
            else
                MaterialManager.Instance.StartCoroutine(RunCPU(callback));
        }

        public void GetStencilUVTexture(MadeMaterialCallback<Texture> callback, bool preview = false)
        {
            if (!preview)
                Rotate();
            else
                _logoTextureRotated = _logoTexture;


            MaterialManager.Instance.StartCoroutine(RunCPUStencilUV(callback));
        }

        #endregion

        #region bake texture

        void RunShader(MadeMaterialCallback<Texture> callback)
        {
            if (_shader == null)
                _shader = (ComputeShader)Resources.Load("CombineTexturesShader");

            int kernelHandle = _shader.FindKernel("CSMain");

            int width = _sourceTexture.width;
            int height = _sourceTexture.height;

            _resultingRenderTexture = new RenderTexture(width, height, 24);
            _resultingRenderTexture.enableRandomWrite = true;
            _resultingRenderTexture.Create();

            _shader.SetTexture(kernelHandle, "Result", _resultingRenderTexture);
            _shader.SetTexture(kernelHandle, "InputTexture1", _sourceTexture);
            _shader.SetTexture(kernelHandle, "InputTexture2", _logoTextureRotated);

            _offset.x = _positionLogo.x;
            _offset.y = _positionLogo.y;
            _offset.z = _logoTextureRotated.width / _positionLogo.width;
            _offset.w = _logoTextureRotated.height / _positionLogo.height;
            _shader.SetVector("Offset", _offset);

            _shader.SetVector("Max", new Vector2(_positionLogo.x + _positionLogo.width, _positionLogo.y + _positionLogo.height));

            _shader.Dispatch(kernelHandle, width / 8, height / 8, 1);

            //resultingTexture2d = toTexture2D(resultingRenderTexture);
            callback(true, _resultingRenderTexture);
            // GetComponent<Renderer>().material.mainTexture = resultingRenderTexture;
        }

        private IEnumerator RunCPUStencilUV(MadeMaterialCallback<Texture> callback)
        {

            int N_LOOP = (int)1e4; //Spread over several frame, number o pixels per frame

            int x = Mathf.FloorToInt(_positionLogo.x);
            int y = Mathf.FloorToInt(_positionLogo.y);
            int width = Mathf.FloorToInt(_positionLogo.width);
            int height = Mathf.FloorToInt(_positionLogo.height);

            Color32[] pixStencil = new Color32[_sourceTexture.width * _sourceTexture.height];
#if UNITY_EDITOR
            try
            {
                pixStencil = _sourceTexture.GetPixels32();
            }
            catch (Exception e)
            {
                Debug.LogWarning(e);
                SetTextureImporterFormat(_sourceTexture, true);
                pixStencil = _sourceTexture.GetPixels32();
            }
#else
        pixStencil = _sourceTexture.GetPixels32();
#endif




            for (int i = 0; i < pixStencil.Length; i++)
            {
                pixStencil[i] = new Color32(0, 0, 0, 0);
            }
            yield return null;

            //read texture logo

            yield return null;

            //Lerp Pixels
            //var logoHeight = _rotated ? _logoTexture.width : _logoTexture.height;
            //var logoWidth = _rotated ? _logoTexture.height : _logoTexture.width;
            var sourceWidth = _sourceTexture.width;

            int counter = 0;
            for (var j = 0; j < height; j++)
            {
                // var logo_j = j * logoHeight / height;
                for (var i = 0; i < width; i++)
                {
                    //var logo_i = i * logoWidth / width;


                    pixStencil[x + i + sourceWidth * (j + y)] = new Color32(255, 255, 255, 255);
                    if (counter % N_LOOP == 0)
                        yield return null;
                    counter++;
                }
            }



            //apply to stencil texture
            var StencilTexture = new Texture2D(_sourceTexture.width, _sourceTexture.height);
            StencilTexture.SetPixels32(pixStencil);
            yield return null;
            StencilTexture.Apply();

            callback(true, StencilTexture);

        }

        private IEnumerator RunCPU(MadeMaterialCallback<Texture> callback)
        {

            int N_LOOP = (int)1e4; //Spread over several frame, number o pixels per frame

            int x = Mathf.FloorToInt(_positionLogo.x);
            int y = Mathf.FloorToInt(_positionLogo.y);
            int width = Mathf.FloorToInt(_positionLogo.width);
            int height = Mathf.FloorToInt(_positionLogo.height);

            //read source texture
            Color32[] pix1;




#if (UNITY_EDITOR)
            try
            {
                pix1 = _sourceTexture.GetPixels32();
            }
            catch (Exception e)
            {
                Debug.LogWarning(e);
                SetTextureImporterFormat(_sourceTexture, true);
                pix1 = _sourceTexture.GetPixels32();
            }
#else
        pix1 = _sourceTexture.GetPixels32();
#endif

            yield return null;

            //read texture logo
            Color32[] pixLogo = _logoTextureRotated.GetPixels32();
            yield return null;


            //Lerp Pixels
            var logoHeight = _rotated ? _logoTexture.width : _logoTexture.height;
            var logoWidth = _rotated ? _logoTexture.height : _logoTexture.width;
            var sourceWidth = _sourceTexture.width;

            int counter = 0;
            for (var j = 0; j < height; j++)
            {
                var logo_j = j * logoHeight / height;
                for (var i = 0; i < width; i++)
                {
                    var logo_i = i * logoWidth / width;

                    pix1[x + i + sourceWidth * (j + y)] = Color.Lerp(pix1[x + i + sourceWidth * (j + y)], pixLogo[logo_i + logo_j * logoWidth], pixLogo[logo_i + logo_j * logoWidth].a);

                    if (counter % N_LOOP == 0)
                        yield return null;
                    counter++;
                }
            }

            //Apply to new texture
            yield return null;
            var CombinedTexture = new Texture2D(_sourceTexture.width, _sourceTexture.height);
            CombinedTexture.SetPixels32(pix1);

            yield return null;
            CombinedTexture.Apply();

            //apply to stencil texture
            var StencilTexture = new Texture2D(_sourceTexture.width, _sourceTexture.height);

            yield return null;
            StencilTexture.Apply();

            callback(true, CombinedTexture);

        }
        #endregion

        #region prepare Methods

        private void Rotate(MadeMaterialCallback<Texture2D> callback = null)
        {
            Color32[] pixLogo = _logoTexture.GetPixels32();

            _rotated = false;
            //Rotation
            switch (_rotationLogo)
            {
                case LogoRotation.CV_90:
                    pixLogo = RotateTexture90Clockwise(pixLogo, _logoTexture.width, _logoTexture.height);
                    _rotated = true;
                    break;

                case LogoRotation.CCV_90:
                    pixLogo = RotateTexture90CounterClockwise(pixLogo, _logoTexture.width, _logoTexture.height);
                    _rotated = true;
                    break;

                case LogoRotation.FLIP:
                    RotateTexture180(ref pixLogo);
                    _rotated = false;
                    break;

                case LogoRotation.NONE:
                    _logoTextureRotated = _logoTexture;
                    if (callback != null)
                        callback(true, _logoTextureRotated);
                    return;
            }


            //Lerp Pixels
            var logoHeight = _rotated ? _logoTexture.width : _logoTexture.height;
            var logoWidth = _rotated ? _logoTexture.height : _logoTexture.width;

            _logoTextureRotated = new Texture2D(logoWidth, logoHeight);
            _logoTextureRotated.SetPixels32(pixLogo);

            _logoTextureRotated.Apply();

            if (callback != null)
                callback(true, _logoTextureRotated);
        }

#if (UNITY_EDITOR)
        private static void SetTextureImporterFormat(Texture2D texture, bool isReadable)
        {
            if (null == texture) return;

            string assetPath = AssetDatabase.GetAssetPath(texture);
            var tImporter = AssetImporter.GetAtPath(assetPath) as TextureImporter;
            if (tImporter != null)
            {
                tImporter.textureType = TextureImporterType.Default;

                tImporter.isReadable = isReadable;

                AssetDatabase.ImportAsset(assetPath);
                AssetDatabase.Refresh();
            }
        }
#endif
        private Color32[] RotateTexture90Clockwise(Color32[] tex, int wid, int hi)
        {
            Color32[] ret = new Color32[wid * hi];      //reminder we are flipping these in the target

            for (int y = 0; y < hi; y++)
            {
                for (int x = 0; x < wid; x++)
                {
                    ret[y + (wid - 1 - x) * hi] = tex[x + y * wid];         //juggle the pixels around


                }
            }

            return ret;
        }

        private Color32[] RotateTexture90CounterClockwise(Color32[] tex, int wid, int hi)
        {
            Color32[] ret = new Color32[wid * hi];      //reminder we are flipping these in the target

            for (int y = 0; y < hi; y++)
            {
                for (int x = 0; x < wid; x++)
                {
                    ret[(hi - 1) - y + x * hi] = tex[x + y * wid];         //juggle the pixels around
                }
            }

            return ret;
        }

        private void RotateTexture180(ref Color32[] tex)
        {
            System.Array.Reverse(tex, 0, tex.Length);
        }

        #endregion
    }
}
