﻿#if (UNITY_EDITOR)
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

namespace Immersely
{
    [ExecuteInEditMode]
    public class Immersely_Container : MonoBehaviour
    {
        #region public fields & properties   

        public string[] KeyOptions;
        public string[] DescritionOptions;
        private AssetObjectContent[] AssetOptions;
        public int ChoiceIndex;
        public Texture2D SourceTexture;
        public Rect PositionLogo;
        public LogoRotation RotationLogo;
        public bool Catching;
        public Texture2D LogoTexture;
        public bool NoKeyFound = true;

        public string Key { get { return KeySelected && AssetOptions != null ? AssetOptions[ChoiceIndex - 1].Key : ""; } }   //TODO out of range exception
        public bool KeySelected { get { return ChoiceIndex > 0; } }
        public bool MaterialRegistered { get { return _list.HasKey(Key); } }
        public string CurrentDescription { get { return DescritionOptions[ChoiceIndex]; } }
        public string CurrentKeyName { get { return KeyOptions[ChoiceIndex]; } }
        public string ErrorMessage
        {
            get
            {
                string s = "The following errors have been found: \n · ";
                for (int i = 0; i < ErrorMessages.Length; i++)
                {
                    s += ErrorMessages[i];
                    if (i < ErrorMessages.Length - 1)
                        s += "\n · ";
                }
                return s;
            }
        }

        //private Vector3[] _framePoints;

        #endregion

        #region private fields
        private LogoRotation _rotationLogoAcumulated;
        private MaterialList _list;
        private bool _rotated;
        private Texture2D _originalSourceTexture;
        private Material _originalMaterial;
        private Collider[] _colliders;
        private bool[] _collidersEnabled;
        private MeshCollider _meshColliderUV;
        private UV_Catcher _catcher;
        static string _assetPath;
        private bool _cleanedUp;
        //error types
        private bool _noInternet;
        private bool _noPrefab;
        private bool _tooManyPrefabs;
        private bool _noKey;
        private bool _noPlacement;
        private Texture2D StencilTexture;

        private string[] ErrorMessages
        {
            get
            {
                List<string> ems = new List<string>();
                if (_noInternet)
                    ems.Add("No internet connection");
                if (_noPrefab)
                    ems.Add("Add Defyu_AdsManager prefab to current scene ");
                if (_tooManyPrefabs)
                    ems.Add("More than one Defyu_AdsManager present in scene");
                if (_noKey)
                    ems.Add("Copy you App id from the admin to the Defyu_AdsManager");
                if (_noPlacement)
                    ems.Add("Add at least one placement in the admin panel");

                return ems.ToArray();
            }
        }

        public bool HasMaterial
        {
            get
            {
                return Immersely_AdsManager.Instance.HasMaterial(Key);
            }
        }
        #endregion

        #region Initialization

        private void OnEnable()
        {
            Initialize();
        }

        public void Initialize()
        {
            DestroyPreviousGarbage();
            GetKeys();
            GetAssetPathList();
            GetOriginalMaterial();

        }

        private void GetOriginalMaterial()
        {
            if (GetComponent<Renderer>() != null)
            {
                _originalSourceTexture = GetComponent<Renderer>().sharedMaterial.mainTexture as Texture2D;
                _originalMaterial = GetComponent<Renderer>().sharedMaterial;
            }
        }

        private void GetKeys()
        {

            AssetOptions = Immersely_AdsManager.GetAllAssets();
            if (AssetOptions.Length == 0)
            {
                NoKeyFound = true;
                StartCoroutine(CheckOK());
                return;
            }
            SetAllOK();

            KeyOptions = new string[AssetOptions.Length + 1];
            DescritionOptions = new string[AssetOptions.Length + 1];

            KeyOptions[0] = "...";
            DescritionOptions[0] = "Please choose a key";

            for (int i = 0; i < AssetOptions.Length; i++)
            {
                KeyOptions[i + 1] = AssetOptions[i].Name;
                DescritionOptions[i + 1] = AssetOptions[i].Description;
            }
        }

        private IEnumerator CheckOK()
        {
            while (NoKeyFound)
            {
                yield return new WaitForSeconds(0.5f);
                _noInternet = false;
                _noPrefab = false;
                _tooManyPrefabs = false;
                _noKey = false;


                var nPrefabs = FindObjectsOfType<Immersely_AdsManager>().Length;
                _noPrefab = nPrefabs == 0;
                _tooManyPrefabs = nPrefabs > 1;

                if (nPrefabs == 1)
                {
                    _noKey = !(FindObjectOfType<Immersely_AdsManager>().ApplicationKey.Length > 0);
                }
                yield return Ping();


                if (!_noInternet && !_noPrefab && !_tooManyPrefabs && !_noKey) //TODO check server if really no placement
                {
                    Immersely_AdsManager.Reset();

                    _noPlacement = true;
                    yield break;
                }
            }
            Initialize();
        }

        private void SetAllOK()
        {
            _noInternet = false;
            _noPrefab = false;
            _tooManyPrefabs = false;
            _noKey = false;
            _noPlacement = false;
            NoKeyFound = false;
        }

        private IEnumerator Ping()
        {
            var adobeRequest = new UnityWebRequest("http://www.adobe.com");
            yield return adobeRequest;
            _noInternet = adobeRequest.isNetworkError;
        }

        private void GetAssetPathList()
        {
            if (string.IsNullOrEmpty(_assetPath))
                _assetPath = GetAssetPath();
            _list = AssetDatabase.LoadAssetAtPath<MaterialList>(_assetPath);
        }

        private void DestroyPreviousGarbage()
        {
            if (GetComponent<MaterialSetter>() != null)
                DestroyImmediate(GetComponent<MaterialSetter>());
        }

        private string GetAssetPath()
        {
            var assets = AssetDatabase.FindAssets("MaterialList");
            return AssetDatabase.GUIDToAssetPath(assets[0]);

        }

        private void OnDestroy()
        {
            CleanUp(true);
        }

        #endregion

        #region Set default values

        private void SetDefault()
        {
            //TODO Material sin textura
            SourceTexture = _originalSourceTexture;
            PositionLogo = new Rect(0, 0, SourceTexture.width, SourceTexture.height);
            RotationLogo = LogoRotation.NONE;
        }

        private void SetFromList()
        {
            var content = _list.GetMaterial(Key);
            SourceTexture = content.SourceTexture;
            PositionLogo = content.PositionLogo;
            RotationLogo = content.RotationLogo;

            Clicked_Preview(false);
        }

        #endregion

        #region Button events

        public void OnKeySelected(int choice)
        {
            ChoiceIndex = choice;
            if (ChoiceIndex == 0)
            {
                return;
            }

            if (!_list.HasKey(Key))
            {
                SetDefault();
                return;
            }
            SetFromList();
        }

        public void Rotate90CCV()
        {
            RotationLogo = LogoRotation.CCV_90;
            _rotationLogoAcumulated--;

            if ((int)_rotationLogoAcumulated < 0)
                _rotationLogoAcumulated += 4;

            SetRotation();
        }

        public void Rotate180()
        {
            RotationLogo = LogoRotation.FLIP;
            _rotationLogoAcumulated += 2;
            if ((int)_rotationLogoAcumulated > 3)
                _rotationLogoAcumulated -= 4;

            SetRotation();
        }

        public void Rotate90CV()
        {
            Debug.Log("RotationLogo pre " + RotationLogo);
            RotationLogo = LogoRotation.CV_90;
            _rotationLogoAcumulated++;
            if ((int)_rotationLogoAcumulated > 3)
                _rotationLogoAcumulated -= 4;

            SetRotation();
            Debug.Log("RotationLogo post " + RotationLogo);
        }

        public void Clicked_MakeMaterial()
        {
            var mm = new MaterialMaker(LogoTexture, new MaterialContainerStruct()
            {
                Key = this.Key,
                SourceTexture = this.SourceTexture,
                PositionLogo = this.PositionLogo,
                RotationLogo = this.RotationLogo,
                //  StencilTexture = this.StencilTexture,
                //StencilKey = MaterialManager.GetStencilKeyEditor(),
            });

            mm.GetStencilUVTexture((bool success, Texture combinedTexture) =>
            {
                if (success)
                {
                    StencilDone(combinedTexture, true);
                }

            }, true);


        }

        public void Clicked_OverWrite()
        {
            var mm = new MaterialMaker(LogoTexture, new MaterialContainerStruct()
            {
                Key = this.Key,
                SourceTexture = this.SourceTexture,
                PositionLogo = this.PositionLogo,
                RotationLogo = this.RotationLogo,
                //  StencilTexture = this.StencilTexture,
                //StencilKey = MaterialManager.GetStencilKeyEditor(),
            });

            mm.GetStencilUVTexture((bool success, Texture combinedTexture) =>
            {
                if (success)
                {
                    StencilDone(combinedTexture, false);
                }

            }, true);
        }

        public void Clicked_ApplyEmpty()
        {
            AddEmptyToMaterialList();
            Finished();
        }

        public void Clicked_Preview(bool rotated = true)
        {
            if (LogoTexture == null)
                LogoTexture = Resources.Load<Texture2D>("TestLogoBig") as Texture2D;

            var mm = new MaterialMaker(LogoTexture, new MaterialContainerStruct()
            {
                Key = this.Key,
                SourceTexture = this.SourceTexture,
                PositionLogo = this.PositionLogo,
                RotationLogo = this.RotationLogo,
                //  StencilTexture = this.StencilTexture,
                //StencilKey = MaterialManager.GetStencilKeyEditor(),
            });

            mm.GetCombinedTexture((bool success, Texture combinedTexture) =>
            {
                if (success)
                {
                    SetMaterial(combinedTexture);
                }

            }, rotated);
        }

        public void Clicked_CatchUV()
        {
            _catcher = gameObject.AddComponent<UV_Catcher>();
            //disable colliders
            _colliders = GetComponentsInChildren<Collider>();
            _collidersEnabled = new bool[_colliders.Length];
            for (int i = 0; i < _colliders.Length; i++)
            {
                _collidersEnabled[i] = _colliders[i].enabled;
                _colliders[i].enabled = false;
            }

            //add meshcollider
            _meshColliderUV = gameObject.AddComponent<MeshCollider>();
            _meshColliderUV.sharedMesh = GetComponent<MeshFilter>().sharedMesh;

            Catching = true;

            // _catcher.SetHandlesFromUV(GetUvPointsHandles(), GetFramePointsHandles());
        }

        public void Clicked_UV_Finished()
        {
            for (int i = 0; i < _colliders.Length; i++)
            {
                _colliders[i].enabled = _collidersEnabled[i];
            }

            DestroyImmediate(_catcher);
            DestroyImmediate(_meshColliderUV);

            Catching = false;
        }

        public void Clicked_Cancel()
        {
            CleanUp(true);
        }

        public void Clicked_AdminPanel()
        {
            Application.OpenURL(Immersely_AdsManager.AdminUrl);
        }

        public void Clicked_Apply()
        {
            Finished();
        }

        #endregion

        #region button Consequences

        private void StencilDone(Texture StencilTexture, bool newMaterial)
        {

            SetStencilMaterial(StencilTexture);

            if (newMaterial)
                MakeNewMaterial();
            else
                OverrideMaterial();
        }

        private void MakeNewMaterial()
        {
            //add key to list
            if (!_list.HasKey(Key))
            {
                string path = "Assets/Immersely/_Textures/StencilMasks/" + CurrentKeyName + ".asset";
                _list.AddMaterial(new MaterialContainerStruct()
                {
                    Key = this.Key,
                    SourceTexture = this.SourceTexture,
                    PositionLogo = this.PositionLogo,
                    RotationLogo = this._rotationLogoAcumulated,
                    StencilTexture = (Texture2D)(AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D))),
                    StencilKey = _list.StencilIndexOf(Key),
                }
                );
            }


            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();
            EditorUtility.SetDirty(_list);

            Finished();
        }

        private void OverrideMaterial()
        {
            if (_list.HasKey(Key))
            {
                string path = "Assets/Immersely/_Textures/StencilMasks/" + CurrentKeyName + ".asset";
                _list.SubsitituteMaterial(new MaterialContainerStruct()
                {
                    Key = this.Key,
                    SourceTexture = this.SourceTexture,
                    PositionLogo = this.PositionLogo,
                    RotationLogo = this._rotationLogoAcumulated,
                    StencilTexture = (Texture2D)(AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D))),
                    StencilKey = _list.StencilIndexOf(Key),
                }
                );
            }

            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();
            EditorUtility.SetDirty(_list);

            Finished();
        }

        private void AddEmptyToMaterialList()
        {
            if (!_list.HasKey(Key))
            {
                _list.SubsitituteMaterial(new MaterialContainerStruct()
                {
                    Key = this.Key,
                    StencilKey = _list.StencilIndexOf(Key),
                }
               );
            }
            else
            {
                _list.AddMaterial(new MaterialContainerStruct()
                {
                    Key = this.Key,
                    StencilKey = _list.StencilIndexOf(Key),
                }
                );
            }

            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();
            EditorUtility.SetDirty(_list);
        }
        private void SetStencilMaterial(Texture StencilTexture)
        {

            if (/*StencilTexture == null ||*/ !UnityEditor.AssetDatabase.Contains(StencilTexture))
            {
                string path = "Assets/Immersely/_Textures/StencilMasks/" + CurrentKeyName + ".asset";


                AssetDatabase.CreateAsset(StencilTexture, path);

                AssetDatabase.ImportAsset(path);
                AssetDatabase.Refresh();
                AssetDatabase.SaveAssets();
                EditorUtility.SetDirty(this);
                // UnityEditor.AssetDatabase.AddObjectToAsset(StencilTexture, _list);
            }
            else
            {

                string path = "Assets/Immersely/_Textures/StencilMasks/" + CurrentKeyName + ".asset";
                UnityEditor.EditorUtility.CopySerialized(StencilTexture, StencilTexture);
                AssetDatabase.ImportAsset(path);
                AssetDatabase.Refresh();
                AssetDatabase.SaveAssets();
                EditorUtility.SetDirty(this);
                // Debug.Log("Updated");
            }
        }

        private void SetMaterial(Texture mainTexture)
        {

            GetComponent<Renderer>().material.mainTexture = mainTexture;

        }

        private void SetCumulatedRotation()
        {
            switch (RotationLogo)
            {
                case LogoRotation.CCV_90:
                    _rotationLogoAcumulated--;
                    break;
                case LogoRotation.CV_90:
                    _rotationLogoAcumulated++;
                    break;
                case LogoRotation.FLIP:
                    _rotationLogoAcumulated += 2;
                    break;
            }
            if ((int)_rotationLogoAcumulated > 3)
                _rotationLogoAcumulated -= 3;
            if ((int)_rotationLogoAcumulated < 0)
                _rotationLogoAcumulated += 3;
        }

        private void SetRotation()
        {


            if (LogoTexture == null)
                LogoTexture = Resources.Load<Texture2D>("TestLogo") as Texture2D;

            var mm = new MaterialMaker(LogoTexture, new MaterialContainerStruct()
            {
                Key = this.Key,
                SourceTexture = this.SourceTexture,
                PositionLogo = this.PositionLogo,
                RotationLogo = this.RotationLogo
            });

            mm.DoRotation((bool success, Texture2D combinedTexture) =>
            {
                if (success)
                {
                    LogoTexture = combinedTexture;
                }

            });
            Clicked_Preview();
        }

        private void Finished()
        {
            //material setter
            var materialSetter = gameObject.AddComponent<MaterialSetter>();
            materialSetter.SetUp(Key, CurrentKeyName);

            CleanUp();
        }

        private void CleanUp(bool canceled = false)
        {
            if (_cleanedUp)
                return;
            _cleanedUp = true;

            if (Catching)
                Clicked_UV_Finished();

            if (GetComponent<Renderer>() != null)
            {
                _originalSourceTexture = GetComponent<Renderer>().sharedMaterial.mainTexture as Texture2D;
                // if(canceled)
                GetComponent<Renderer>().material = _originalMaterial;
            }

            DestroyImmediate(this);

        }

        internal void SetUVs(Rect rectPosistion)
        {
            PositionLogo = rectPosistion;
            PositionLogo.x = Mathf.Round(PositionLogo.x);
            PositionLogo.y = Mathf.Round(PositionLogo.y);
            PositionLogo.width = Mathf.Round(PositionLogo.width);
            PositionLogo.height = Mathf.Round(PositionLogo.height);

            //SetResolution_SQ();

            Clicked_Preview();

            // _framePoints = framePoints;
        }

        private void SetResolution_SQ()
        {
            float centerX = PositionLogo.x + PositionLogo.width / 2f;
            float centerY = PositionLogo.y + PositionLogo.height / 2f;

            float side = (PositionLogo.width + PositionLogo.height) / 2f;

            PositionLogo.x = Mathf.Round(centerX - side / 2);
            PositionLogo.y = Mathf.Round(centerY - side / 2);

            PositionLogo.width = Mathf.Round(side);
            PositionLogo.height = Mathf.Round(side);

        }

        private Vector3[] GetFramePoints()
        {
            Vector3[] fps = new Vector3[4];
            var uvWorld = gameObject.AddComponent<UVWorld>();
            uvWorld.Init();

            Vector2[] uvPoints = new Vector2[4];
            uvPoints[0] = new Vector2(PositionLogo.x, PositionLogo.y) / 1024f;
            uvPoints[1] = new Vector2(PositionLogo.x + PositionLogo.width, PositionLogo.y + PositionLogo.height) / 1024f;
            uvPoints[2] = new Vector2(PositionLogo.x + PositionLogo.width, PositionLogo.y) / 1024f;
            uvPoints[3] = new Vector2(PositionLogo.x, PositionLogo.y + PositionLogo.height) / 1024f;

            for (int i = 0; i < 4; i++)
            {
                fps[i] = new Vector3();
                Vector3 normal;
                uvWorld.World(uvPoints[i], out fps[i], out normal);
            }
            DestroyImmediate(uvWorld);
            return fps;
        }

        public Vector3[] GetFramePointsHandles()
        {
            Vector3[] fps = new Vector3[4];
            var uvWorld = gameObject.AddComponent<UVWorld>();
            uvWorld.Init();

            Vector2[] uvPoints = new Vector2[4];
            uvPoints[0] = new Vector2(PositionLogo.x, PositionLogo.y) / 1024f;
            uvPoints[2] = new Vector2(PositionLogo.x + PositionLogo.width, PositionLogo.y + PositionLogo.height) / 1024f;
            uvPoints[3] = new Vector2(PositionLogo.x + PositionLogo.width, PositionLogo.y) / 1024f;
            uvPoints[1] = new Vector2(PositionLogo.x, PositionLogo.y + PositionLogo.height) / 1024f;

            for (int i = 0; i < 4; i++)
            {
                fps[i] = new Vector3();
                Vector3 normal;
                uvWorld.World(uvPoints[i], out fps[i], out normal);
            }
            DestroyImmediate(uvWorld);
            return fps;
        }

        internal Vector2[] GetUvPointsHandles()
        {
            Vector2[] uvPoints = new Vector2[4];
            uvPoints[0] = new Vector2(PositionLogo.x, PositionLogo.y);
            uvPoints[2] = new Vector2(PositionLogo.x + PositionLogo.width, PositionLogo.y + PositionLogo.height);
            uvPoints[3] = new Vector2(PositionLogo.x + PositionLogo.width, PositionLogo.y);
            uvPoints[1] = new Vector2(PositionLogo.x, PositionLogo.y + PositionLogo.height);

            return uvPoints;
        }
        #endregion

    }
}

#endif
