﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Immersely
{
    public class DownloadManager : MonoBehaviour
    {

        #region singleton
        private static DownloadManager _instance;
        public static DownloadManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<DownloadManager>();

                    if (_instance == null)
                    {
                        GameObject go = new GameObject();
                        go.name = "DownloadManager";
                        _instance = go.AddComponent<DownloadManager>();
#if !UNITY_EDITOR
                    DontDestroyOnLoad(go);
#endif
                    }
                }

                return _instance;
            }
        }
        #endregion

        #region fields

        private List<AssetObjectContent> _todoList = new List<AssetObjectContent>();
        private UnityWebRequest _www;
        private Texture2D _texture;
        private Rect _rec = new Rect(0, 0, 0, 0);
        private bool _working;
        private static Dictionary<string, Texture2D> _memoryCache = new Dictionary<string, Texture2D>();

        #endregion

        #region methods
        

        void Update()
        {
            if (!_working && _todoList.Count > 0)
                DownloadNext();
        }

        internal static void AddContent(AssetObjectContent content)
        {
            Instance._todoList.Add(content);
            content.SetWaiting();
        }

        private void DownloadNext()
        {
            StartCoroutine(DoDownload(_todoList[0]));
            _todoList.Remove(_todoList[0]);
        }

        private IEnumerator DoDownload(AssetObjectContent content)
        {
            _working = true;
            yield return StartCoroutine(CatchImage(content));
            _working = false;
        }

        private IEnumerator CatchImage(AssetObjectContent content)
        {
            content.SetDownloadInProgress();
            string path = content.Path;


            // get from cache if available
            if (_memoryCache.ContainsKey(path))
            {
                content.DownloadFinished(_memoryCache[path]);
                yield break;
            }


            _www = UnityWebRequestTexture.GetTexture(path);
            yield return _www.SendWebRequest();

            if (_www.isNetworkError)
            {
                content.DownLoadFailed();

                _www.Dispose();
                _www = null;
                yield break;
            }
            else
            {
                try
                {
                    _texture = ((DownloadHandlerTexture)_www.downloadHandler).texture;
                    _rec.width = _texture.width;
                    _rec.height = _texture.height;

                    content.DownloadFinished(_texture);

                    _www.Dispose();
                    _www = null;

                    if (!_memoryCache.ContainsKey(path))
                        _memoryCache.Add(path, _texture);
                }
                catch (InvalidOperationException ex)
                {
                    Debug.LogError(ex);
                    content.DownLoadFailed();
                }
            }
        }

        #endregion
    }
}


