﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

namespace Immersely
{
    public class Immersely_Analytics : MonoBehaviour
    {
       

        #region singleton
        private static Immersely_Analytics _instance;
        public static Immersely_Analytics Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<Immersely_Analytics>();

                    if (_instance == null)
                    {
                        GameObject go = new GameObject();
                        go.name = "DefyuAdsManager";
                        _instance = go.AddComponent<Immersely_Analytics>();
#if !UNITY_EDITOR
                    DontDestroyOnLoad(go);
#endif
                    }
                }

                return _instance;
            }
        }


        #endregion

        #region general fields

        /*
         * The render texture will be downscaled by this factor
         */
        public float reductor = 8;
        Dictionary<string, float> AccumulatedImpactValues;
        private Dictionary<string, float> _timeSinceLastCalc;
        private Dictionary<string, float> _timeSinceLastSend;

        private float CentralityFactor = 0.1f;
        private float a_ScreenToImpact = 1;
        private float b_ScreenToImpact = 0;

        private float _calcFrequency = 0.1f;
        private float _sendFrequency = 2;
        private bool _shouldTrack;

        private float _timeSinceLastCalcStencil;
        private string[] _keys;

        #endregion

        #region stencil fields
        public Material[] PostprocessMaterial;
        public RenderTexture StencilTempTexture;
        private RenderTexture StencilTempTexture_lowres;
        private RenderTargetIdentifier StencilTempTextureID;
        private CommandBuffer cmdBuffer;
        private ComputeShader _computeShaderCalcPercentage;
        static Mesh s_Quad;
        public Camera MainCamera;
        private Camera activeCameraCPU;
        private Texture2D tex;
        private Dictionary<string, float> CurrentStencilScreenPercentage;
        private Dictionary<string, int> MaterialKeys;
        private Dictionary<string, int> StencilKeys;
        private bool _busyCalculating;
        int[] vals;
        int[] sums;
        string[] keys;
        RenderBuffer[] _mrt;

        private float _totalPixels;
        #endregion

        #region Initialize

        private void Awake()
        {
            _instance = this;
        }

        private void OnEnable()
        {
            MaterialManager.MaterialPreparedDelegate += OnInitialized;
        }

        private void OnDisable()
        {
            MaterialManager.MaterialPreparedDelegate -= OnInitialized;
        }

        private void OnInitialized()
        {
            _keys = MaterialManager.MaterialKeys.ToArray();


            AccumulatedImpactValues = new Dictionary<string, float>();
            foreach (var key in _keys)
            {
                AccumulatedImpactValues.Add(key, 0);
            }

            _timeSinceLastCalc = new Dictionary<string, float>();
            foreach (var key in _keys)
            {
                _timeSinceLastCalc.Add(key, 0);
            }

            _timeSinceLastSend = new Dictionary<string, float>();
            foreach (var key in _keys)
            {
                _timeSinceLastSend.Add(key, 0);
            }



            CurrentStencilScreenPercentage = new Dictionary<string, float>();
            foreach (var key in _keys)
            {
                CurrentStencilScreenPercentage.Add(key, 0);
            }

            StencilKeys = MaterialManager.StencilKeys;


            SetCommandBuffer();
            InitializeCPUcalc();
            _shouldTrack = true;

        }

        #endregion

        #region analize visibility
        private void DoAnal()
        {
            if (_shouldTrack)
            {

                StartCoroutine(TryCalculate());

            }
        }

        private IEnumerator TryCalculate()
        {
            _timeSinceLastCalcStencil += Time.deltaTime;

            if (_timeSinceLastCalcStencil > _calcFrequency && !_busyCalculating)
            {
                yield return StartCoroutine(UpdateStencilTest());
                foreach (var key in _keys)
                {
                    TryCalculate(key);
                }
                _timeSinceLastCalcStencil = 0;
            }
            TrySend();
        }

        private void TrySend()
        {
            foreach (var key in _keys)
            {
                TrySend(key);
            }
        }

        private void TryCalculate(string key)
        {
            UpdateImpactStencil(key);

        }

        private void TrySend(string key)
        {
            _timeSinceLastSend[key] += Time.deltaTime;
            if (_timeSinceLastSend[key] > _sendFrequency)
            {
                SendVisual(key, AccumulatedImpactValues[key], gameObject.GetHashCode(),Immersely_AdsManager.IsContracted(key));
                _timeSinceLastSend[key] = 0;
            }
        }

        internal static void SendVisual(string key, float cumulatedArea, int v, bool contracted)
        {
            Debug.Log("send: " + key + "  " + cumulatedArea+"   "+contracted);
          
            WebApiClient.Request_ScreenUsagePercentByTime(key, cumulatedArea, v.ToString());
        }

        private void UpdateImpactStencil(string key)
        {
            float currentImpact = GetCurrentImpact(new float[] { CurrentStencilScreenPercentage[key], 1 });
            SetAccumulatedImpact(key, currentImpact);
        }

        private void SetAccumulatedImpact(string key, float currentImpact)
        {
            AccumulatedImpactValues[key] += currentImpact * _timeSinceLastCalcStencil;
        }

        private float GetCurrentImpact(List<float[]> rawValues)
        {
            float sum = 0;
            foreach (var item in rawValues)
            {
                sum += ScreenPercentageToImpact(item[0]) * CentralityToImpact(item[1]);
            }
            return sum;
        }

        private float GetCurrentImpact(float[] rawValues)
        {
            float sum = 0;
            foreach (var item in rawValues)
            {
                sum += ScreenPercentageToImpact(rawValues[0]) * CentralityToImpact(rawValues[1]);
            }
            return sum;
        }

        private float ScreenPercentageToImpact(float screenPercentage)
        {
            return Mathf.Clamp01(a_ScreenToImpact * screenPercentage + b_ScreenToImpact);
        }

        private float CentralityToImpact(float relativeDistanceToCenterScreen)
        {
            return Mathf.Clamp01(1 - relativeDistanceToCenterScreen * CentralityFactor);
        }



        #endregion

        #region stencil Test

        public static Mesh quad
        {
            get
            {
                if (s_Quad != null)
                    return s_Quad;

                var vertices = new[]
                {
                new Vector3(-0.5f, -0.5f, 0f),
                new Vector3(0.5f,  0.5f, 0f),
                new Vector3(0.5f, -0.5f, 0f),
                new Vector3(-0.5f,  0.5f, 0f)
            };

                var uvs = new[]
                {
                new Vector2(0f, 0f),
                new Vector2(1f, 1f),
                new Vector2(1f, 0f),
                new Vector2(0f, 1f)
            };

                var indices = new[] { 0, 1, 2, 1, 0, 3 };

                s_Quad = new Mesh
                {
                    vertices = vertices,
                    uv = uvs,
                    triangles = indices
                };
                s_Quad.RecalculateNormals();
                s_Quad.RecalculateBounds();

                return s_Quad;
            }
        }

        void Update()
        {
            DoAnal();
        }

        void SetCommandBuffer()
        {

            if (MainCamera == null)
                MainCamera = Camera.main;

            if (MainCamera == null)
                Debug.LogError("No main camera could be found!");


            PostprocessMaterial = new Material[MaterialManager.NumberOfContainers];
            MaterialKeys = new Dictionary<string, int>();
            int s = 0;
            foreach (var item in StencilKeys)
            {
                PostprocessMaterial[s] = new Material(Shader.Find("Defyu/StencilToRT"));
                PostprocessMaterial[s].SetInt("_StencilMask", item.Value);

                MaterialKeys.Add(item.Key, s);
                s++;
            }

            StencilTempTexture = new RenderTexture(MainCamera.pixelWidth, MainCamera.pixelHeight, 24);

            StencilTempTexture.name = "Stencil";
            StencilTempTextureID = new RenderTargetIdentifier(StencilTempTexture);
            StencilTempTexture.filterMode = FilterMode.Point;

            StencilTempTexture_lowres = new RenderTexture(Mathf.RoundToInt(MainCamera.pixelWidth / reductor), Mathf.RoundToInt(MainCamera.pixelHeight / reductor), 24);
            StencilTempTexture_lowres.filterMode = FilterMode.Point;



            if (cmdBuffer == null)
            {
                cmdBuffer = new CommandBuffer();
                cmdBuffer.name = "cmdBuffer";

                // now we have a RT from the camera render we can reuse the depth / stencil from that
                cmdBuffer.SetRenderTarget(StencilTempTextureID, BuiltinRenderTextureType.CameraTarget);

                // clear just the color
                cmdBuffer.ClearRenderTarget(false, true, Color.black);

                // We can't use blit here as it will internal set destination,
                // so draw a quad instead. Vertex shader in the shader has been changed
                // see the code attached.

                int sk = 0;
                foreach (var item in StencilKeys)
                {
                    cmdBuffer.DrawMesh(quad, Matrix4x4.identity, PostprocessMaterial[sk], 0, 0);
                    sk++;
                }




                // downsample screen copy into smaller RT, release screen RT
                cmdBuffer.Blit(StencilTempTexture, StencilTempTexture_lowres);





                MainCamera.AddCommandBuffer(CameraEvent.BeforeImageEffects, cmdBuffer);



            }
        }

        IEnumerator UpdateStencilTest()
        {
            //   if (SystemInfo.graphicsShaderLevel >= 45)
            //     yield return StartCoroutine(DoComputeShaderCalc());
            //  else
            yield return StartCoroutine(DoCPUCalc());

        }

        private void InitializeCPUcalc()
        {
            if (activeCameraCPU == null)
            {
                // activeCameraCPU = (Camera)Camera.Instantiate(MainCamera, Vector3.zero, Quaternion.identity);
                activeCameraCPU = gameObject.AddComponent<Camera>();
            }

            activeCameraCPU.depth = -100;
            Destroy(activeCameraCPU.GetComponent<AudioListener>());

            tex = new Texture2D(Mathf.RoundToInt(MainCamera.pixelWidth / reductor), Mathf.RoundToInt(MainCamera.pixelHeight / reductor), TextureFormat.RGB24, false);

            StencilKeys = StencilKeys.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
            vals = StencilKeys.Values.ToArray();

            sums = new int[vals.Length];
            keys = StencilKeys.Keys.ToArray();

            _totalPixels = (MainCamera.pixelWidth / reductor) * (MainCamera.pixelHeight / reductor);

        }

        private IEnumerator DoCPUCalc()
        {

            _busyCalculating = true;
            RenderTexture.active = StencilTempTexture_lowres;

            // Read pixels
            tex.ReadPixels(new Rect(0, 0, Mathf.RoundToInt(MainCamera.pixelWidth / reductor), Mathf.RoundToInt(MainCamera.pixelHeight / reductor)), 0, 0);


            // Clean up
            activeCameraCPU.targetTexture = null;
            RenderTexture.active = null; // added to avoid errors 

            yield return null;
            var pixels = tex.GetPixels32();

            yield return null;

            sums = new int[vals.Length];

            for (int i = 0; i < pixels.Length; i++)
            {
                for (int v = 0; v < vals.Length; v++)
                {
                    if (pixels[i].r == vals[v])
                        sums[v]++;
                }
            }

            for (int i = 0; i < vals.Length; i++)
            {
                CurrentStencilScreenPercentage[keys[i]] = (float)sums[i] / pixels.Length;


            }
            _busyCalculating = false;

        }

        private IEnumerator DoComputeShaderCalc()
        {
            if (_computeShaderCalcPercentage == null)
                _computeShaderCalcPercentage = (ComputeShader)Resources.Load("CalcFractionScreen");

            int kernelHandle = _computeShaderCalcPercentage.FindKernel("CSMain");

            ComputeBuffer buffer = new ComputeBuffer(4, 4, ComputeBufferType.Counter);

            // buffer.SetData(new uint[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
            buffer.SetData(new uint[] { 0, 0, 0, 0 });
            _computeShaderCalcPercentage.SetBuffer(kernelHandle, "Counter", buffer);


            // _computeShaderCalcPercentage.SetFloats("StencilKeys", new float[] { 10,10,10,10, 30, 30, 30, 30, 20, 20, 20, 20});

            _computeShaderCalcPercentage.SetTexture(kernelHandle, "Stencil", StencilTempTexture_lowres);
            _computeShaderCalcPercentage.Dispatch(kernelHandle, StencilTempTexture_lowres.width, StencilTempTexture_lowres.height, 4);
            yield return null;

            var kk = new uint[keys.Length];
            buffer.GetData(kk);

            buffer.Dispose();



            for (int i = 0; i < vals.Length; i++)
            {
                CurrentStencilScreenPercentage[keys[i]] = (float)kk[i] / _totalPixels;
            }


            // Debug.Log(kk[0] + "  " + kk[1] + "   " + kk[2]);


        }

        #endregion
    }
}
