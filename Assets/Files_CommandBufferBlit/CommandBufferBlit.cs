﻿using UnityEngine;
using UnityEngine.Rendering;
using System.Collections;
using UnityEngine.UI;

public class CommandBufferBlit : MonoBehaviour
{
    public Material[] PostprocessMaterial;
    public Material stencil_is_on;

    private RenderTexture StencilTempTexture;
    private RenderTexture Buffer;
    public RenderTexture StencilTempTexture_lowres;

    private RenderTargetIdentifier StencilTempTextureID;
    private RenderTargetIdentifier BufferID;

    private CommandBuffer cmdBuffer;

    public ComputeShader shader;

    public Material KK;
    public Renderer Cube;
    static Mesh s_Quad;
    public static Mesh quad
    {
        get
        {
            if (s_Quad != null)
                return s_Quad;

            var vertices = new[]
            {
                new Vector3(-0.5f, -0.5f, 0f),
                new Vector3(0.5f,  0.5f, 0f),
                new Vector3(0.5f, -0.5f, 0f),
                new Vector3(-0.5f,  0.5f, 0f)
            };

            var uvs = new[]
            {
                new Vector2(0f, 0f),
                new Vector2(1f, 1f),
                new Vector2(1f, 0f),
                new Vector2(0f, 1f)
            };

            var indices = new[] { 0, 1, 2, 1, 0, 3 };

            s_Quad = new Mesh
            {
                vertices = vertices,
                uv = uvs,
                triangles = indices
            };
            s_Quad.RecalculateNormals();
            s_Quad.RecalculateBounds();

            return s_Quad;
        }
    }

    public Text Text;

    int kkSize = 128;

    public float reductor;

    public Camera kkCam;
public Texture2D tex;
    // NEED TO ADD THIS
    // forces camera to go via an internal RT for rendering
    // it's easier than allocating and managing your own.
    // Any image effect on the camera will remove the need
    // for this.
    // In 5.6 there will be an API on camera for forceIntoRenderTexure
    void OnRenderImage (RenderTexture src, RenderTexture dst)
    {
        Graphics.Blit (src, dst);
       

    }

    //void OnPostRender()
    //{
    void OnEnable()
    {
      
        // moved these to here
        // can still be in OnStart, but I prefer to
        // lifescycle manage in OnEnable
        PostprocessMaterial = new Material[2];
        PostprocessMaterial[0] = new Material(Shader.Find("StencilToBlackAndWhite"));
        PostprocessMaterial[1] = new Material(Shader.Find("StencilToBlackAndWhite"));
        PostprocessMaterial[0].SetInt("_StencilMask", 10);
        PostprocessMaterial[1].SetInt("_StencilMask", 128);
        // We need one temp texture here for the loop,
        // allocation + clearning can be moved into the
        // cmd allocation below unless you want to reuse it.
        StencilTempTexture = new RenderTexture(Screen.width, Screen.height, 24);       
        StencilTempTexture.name = "Stencil";
        StencilTempTextureID = new RenderTargetIdentifier(StencilTempTexture);

        StencilTempTexture_lowres = new RenderTexture(Mathf.RoundToInt(Screen.width/reductor), Mathf.RoundToInt(Screen.height / reductor), 24);
        StencilTempTexture_lowres.filterMode = FilterMode.Bilinear;


       // StencilTempTexture_lowres = new RenderTexture(Screen.width, Screen.height, 24);

        if (cmdBuffer == null)
        {
            cmdBuffer = new CommandBuffer();
            cmdBuffer.name = "cmdBuffer";

            // now we have a RT from the camera render we can reuse the depth / stencil from that
            cmdBuffer.SetRenderTarget(StencilTempTextureID, BuiltinRenderTextureType.CameraTarget);

            // clear just the color
            cmdBuffer.ClearRenderTarget(false, true, Color.black);

            // We can't use blit here as it will internal set destination,
            // so draw a quad instead. Vertex shader in the shader has been changed
            // see the code attached.
            cmdBuffer.DrawMesh(quad, Matrix4x4.identity, PostprocessMaterial[0], 0, 0);
            cmdBuffer.DrawMesh(quad, Matrix4x4.identity, PostprocessMaterial[1], 0, 0);

            // set the global texture
            cmdBuffer.SetGlobalTexture("_StencilTempTexture", StencilTempTexture);

         
            // downsample screen copy into smaller RT, release screen RT
            cmdBuffer.Blit(StencilTempTexture, StencilTempTexture_lowres);
           
            GetComponent<Camera>().AddCommandBuffer(CameraEvent.BeforeImageEffects, cmdBuffer); 

        }
    }
    //}

    

    int counter;
    void Update()
    {
        counter++;
        //DoComputeShaderCalc();

         DoCPUCalc();
       // Text.text = "" + (Time.time / counter);
    }

    private void DoCPUCalc()
    {
         tex = new Texture2D(Mathf.RoundToInt(Screen.width / reductor), Mathf.RoundToInt(Screen.height / reductor), TextureFormat.RGB24, false);

        // ofc you probably don't have a class that is called CameraController :P
        Camera activeCamera =kkCam;

        // Initialize and render
        //RenderTexture rt = new RenderTexture(width, height, 24);

       // activeCamera.targetTexture = StencilTempTexture_lowres;
        //activeCamera.Render();
        RenderTexture.active = StencilTempTexture_lowres;

        // Read pixels
        tex.ReadPixels(new Rect(0,0, Mathf.RoundToInt(Screen.width / reductor), Mathf.RoundToInt(Screen.height / reductor)), 0, 0);
        tex.Apply();
        // Clean up
        activeCamera.targetTexture = null;
        RenderTexture.active = null; // added to avoid errors 

        var kk = tex.GetPixels(0, 0, Mathf.RoundToInt(Screen.width / reductor), Mathf.RoundToInt(Screen.height / reductor));
        float[] sum = new float[] { 0, 0 };
        for (int i = 0; i < kk.Length; i++)
        {
            if(Mathf.Abs(kk[i].r * 255 - 128)<1)
                sum[0] ++ ;
            if (Mathf.Abs(kk[i].r * 255 - 10) < 1)
                sum[1]++;
        }
        float s1 = sum[0] / (Mathf.RoundToInt(Screen.width / reductor) * Mathf.RoundToInt(Screen.height / reductor)) * 100;
        float s2 = sum[1] / (Mathf.RoundToInt(Screen.width / reductor) * Mathf.RoundToInt(Screen.height / reductor)) * 100;
        Text.text = ""+ s1+ "  "+s2+"   "+Time.time / counter;
      //  DestroyImmediate(StencilTempTexture_lowres);
    }

    private void DoComputeShaderCalc()
    {
        //Cube.material.mainTexture = StencilTempTexture;
        // return;
        int kernelHandle = shader.FindKernel("CSMain");

        //RenderTexture tex = new RenderTexture(256, 256, 24);
        //tex.enableRandomWrite = true;
        //tex.Create();



        ComputeBuffer buffer = new ComputeBuffer(2, 4);
        buffer.SetData(new uint[] { 0, 0 });
        shader.SetBuffer(kernelHandle, "Counter", buffer);

        //ComputeBuffer bufferStencil = new ComputeBuffer(1, 4);
        //buffer.SetData(StencilTempTexture.depthBuffer.);
        //shader.SetBuffer(kernelHandle, "Counter", buffer);

        // rt = Shader.GetGlobalTexture("_StencilTempTexture");
        shader.SetTexture(kernelHandle, "Stencil", StencilTempTexture_lowres);
        shader.Dispatch(kernelHandle, Screen.width / 8, Screen.height / 8, 1);

        var kk = new uint[] { 0, 0 };
        buffer.GetData(kk);

        buffer.Dispose();
        
        var f1 = (float)kk[0] / (StencilTempTexture_lowres.width * StencilTempTexture_lowres.height) * 100;
        var f2 = (float)kk[1] / (StencilTempTexture_lowres.width * StencilTempTexture_lowres.height) * 100;
        Text.text = (f1 + "%  " + f2 + "%   " + Time.time / counter);
        //Debug.Log(kk[0] + "  " + kk[1]);
    }
}